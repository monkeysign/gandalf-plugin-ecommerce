<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 15/07/2018
 * Time: 18:38
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_product';
    protected $fillable = array('title','description','rilevance','note','status','price','id_category','id_brand','novelty','offer','price_offer','state_product','picked', 'best_seller','quantity','ean','ean2','file','type','permalink', 'grade', 'mac_powerup', 'amazon', 'sku', "facebook");

    public function attributes() {
        return $this->belongsToMany( 'Plugins\ECOMMERCE\Models\Attribute', 'ecommerce_product_attribute', 'id_product', 'id_attribute' );
    }


    public function categorySec() {
        return $this->belongsToMany( 'Plugins\ECOMMERCE\Models\Category', 'ecommerce_product_category', 'id_product', 'id_category' );
    }


    public function category() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Category', 'id', 'id_category' );
    }

    public function brand() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Brand', 'id', 'id_brand' );
    }

    /**
     * Prende il ProductMeta del post secondo una determinata chiave, o restituisce l'intera collection
     * @param null $key
     * @param null $isoLingua
     *
     * @return $this|mixed
     */
    public function meta( $key = null, $isoLingua = null ) {
        // Lingua di default
        if ( ! $isoLingua ) {
            $isoLingua = config( 'locale' );
        }
        // Se non setto una chiave restituisco tutti i meta
        if ( ! $key ) {
            return $this->hasMany( 'Plugins\ECOMMERCE\Models\ProductMeta', 'id_product', 'id' )->where( 'iso', $isoLingua );
        }
        // se ho la chiave restituisco il valore di quel meta in quella lingua
        $val =  $this->hasMany( 'Plugins\ECOMMERCE\Models\ProductMeta', 'id_product', 'id' )
            ->where( 'meta_key', $key )
            ->where( 'iso', $isoLingua )
            ->first();
        if($val) return $val->value;
        else return false;
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return Product::create($item);
        } else {
            Product::where('id', '=', $item['id'])->update($item);
            return Product::find($item['id']);
        }
    }

    public function get_price(){
        return number_format($this->price, 2, ',', '.');
    }

    public function get_price_offer(){
        return number_format($this->price_offer, 2, ',', '.');
    }

    public function getPermalink(){
        $catPermalink='';
        if($this->category && $this->category->categoryRoot())
            $catPermalink = $this->category->categoryRoot()->permalink;
        $link =$catPermalink . '/'.$this->permalink;
        return $link;
    }


}
