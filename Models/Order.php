<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 07/08/2018
 * Time: 20:32
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_order';
    protected $fillable = array('title','status','payment_status','payment_type','id_customer','note','date','total','payment_date', 'code_payment','tracking','downloadable','refund_date','refund','cost_ship','id_ship', 'email_paypal','user_paypal');

    public function products() {
        return $this->belongsToMany( 'Plugins\ECOMMERCE\Models\Product', 'ecommerce_order_product', 'id_order', 'id_product' );
    }


    public function orderProduct() {
        return $this->hasMany('Plugins\ECOMMERCE\Models\OrderProduct', 'id_order', 'id');
    }


    public function typePayment() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Payment', 'id', 'payment_type' );
    }

    public function typeShip() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Shipment', 'id', 'id_ship' );
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return Order::create($item);
        } else {
            Order::where('id', '=', $item['id'])->update($item);
            return Order::find($item['id']);
        }
    }

    /**
     * Indirizzo di default spedizione
     * @return Eloquent|null|object|static
     */
    public function shipping_address(){
        $val =  $this->hasMany('Plugins\ECOMMERCE\Models\OrderAddress', 'id_order', 'id' )
            ->where('type', 1)
            ->where('selected', 1)
            ->first();

        if($val) return $val;

        else return false;
    }

    /**
     * Indirizzo di default fatturazione
     * @return Eloquent|null|object|static
     */
    public function billing_address(){
        $val =  $this->hasMany('Plugins\ECOMMERCE\Models\OrderAddress', 'id_order', 'id' )
            ->where('type', 2)
            ->where('selected', 1)
            ->first();

        if($val) return $val;

        else return false;
    }
}