<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 15/07/2018
 * Time: 18:38
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderList extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_order';
    protected $fillable = array('id','title','status','name','date', 'surname');


}
