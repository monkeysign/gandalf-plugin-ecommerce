<?php

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderProduct extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_order_product';
    protected $fillable = array('id_order', 'id_product', 'title', 'quantity','vat','price');
    public $incrementing = false;


    public function product() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Product', 'id', 'id_product' );
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return OrderProduct::create($item);
        } else {
            OrderProduct::where('id', '=', $item['id'])->update($item);
            return OrderProduct::find($item['id']);
        }
    }

}
