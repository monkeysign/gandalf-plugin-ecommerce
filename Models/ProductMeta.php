<?php

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ProductMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_product_meta';
    protected $fillable = array('id_product', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function category() {
        return $this->belongsTo('Plugins\ECOMMERCE\Models\Product', 'id', 'id_product');
    }

}
