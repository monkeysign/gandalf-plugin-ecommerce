<?php

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BrandMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_brand_meta';
    protected $fillable = array('id_brand', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function brand() {
        return $this->belongsTo('Plugins\ECOMMERCE\Models\Brand', 'id', 'id_brand');
    }

}
