<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 02/06/2018
 * Time: 21:26
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;


class Brand extends Eloquent {

    use SoftDeletes;

    protected $dates = [ 'deleted_at' ];
    protected $table = 'ecommerce_brand';
    protected $fillable = array( 'title','status','code','media', 'order');

    /**
     * Prende il BrandMeta del post secondo una determinata chiave, o restituisce l'intera collection
     * @param null $key
     * @param null $isoLingua
     *
     * @return $this|mixed
     */
    public function meta( $key = null, $isoLingua = null ) {
        // Lingua di default
        if ( ! $isoLingua ) {
            $isoLingua = config( 'locale' );
        }
        // Se non setto una chiave restituisco tutti i meta
        if ( ! $key ) {
            return $this->hasMany( 'Plugins\ECOMMERCE\Models\BrandMeta', 'id_brand', 'id' )->where( 'iso', $isoLingua );
        }
        // se ho la chiave restituisco il valore di quel meta in quella lingua
        $val =  $this->hasMany( 'Plugins\ECOMMERCE\Models\BrandMeta', 'id_brand', 'id' )
            ->where( 'meta_key', $key )
            ->where( 'iso', $isoLingua )
            ->first();
        if($val) return $val->value;
        else return false;
    }

    /**
     * Salva un nuovo record o fa l'update
     * @param $item
     *
     * @return \Plugins\CMS\Models\Post
     */
    public static function saveOrUpdate( $item ) {
        if ( ! isset( $item['id'] ) || ! $item['id'] ) {
            return Brand::create( $item );
        } else {
            Brand::where( 'id', $item['id'] )->update( $item );
            return Brand::find( $item['id'] );
        }
    }
}
