<?php
namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_feature';
    protected $fillable = array('title','status');

    public function attributes() {
        return $this->hasMany('Plugins\ECOMMERCE\Models\Attribute', 'id_feature', 'id');
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return Feature::create($item);
        } else {
            Feature::where('id', '=', $item['id'])->update($item);
            return Feature::find($item['id']);
        }
    }

}