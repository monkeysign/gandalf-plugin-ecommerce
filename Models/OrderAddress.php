<?php

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderAddress extends Eloquent {

	protected $table = 'ecommerce_order_address';
	protected $fillable = array('id_order', 'city', 'nation', 'zipcode', 'address', 'phone','state', 'type', 'selected','name','surname','email');

	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			return OrderAddress::create($item);
		} else {
            OrderAddress::where('id', '=', $item['id'])->update($item);
			return OrderAddress::find($item['id']);
		}
	}

	public function order() {
		return $this->belongsTo('Plugins\CRM\Customer\Models\Order', 'id', 'id_order');
	}

}
