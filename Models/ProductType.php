<?php
namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductType extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_product_type';
    protected $fillable = array('title','status','slug');

    public function features() {
        return $this->belongsToMany( 'Plugins\ECOMMERCE\Models\Feature', 'ecommerce_product_type_feature', 'id_product_type', 'id_feature' );
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return ProductType::create($item);
        } else {
            ProductType::where('id', '=', $item['id'])->update($item);
            return ProductType::find($item['id']);
        }
    }
}