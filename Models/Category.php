<?php
namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_category';
    protected $fillable = array('title','status','id_parent','id_product_type', 'search', 'path', 'path_label', 'permalink');


    public function productType() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\ProductType',  'id', 'id_product_type' );
    }

    public function categoryChild() {
        return $this->hasMany( 'Plugins\ECOMMERCE\Models\Category',  'id_parent', 'id' )->where( 'status', 1 )->orderBy('orders', 'asc');
        //return $this->hasMany( 'Plugins\ECOMMERCE\Models\Category',  'id_parent', 'id' )->where( 'status', 1 );
    }

    public function categoryFather() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Category',  'id', 'id_parent' );
    }

    /**
     * Prende il CategoryMeta del post secondo una determinata chiave, o restituisce l'intera collection
     * @param null $key
     * @param null $isoLingua
     *
     * @return $this|mixed
     */
    public function meta( $key = null, $isoLingua = null ) {
        // Lingua di default
        if ( ! $isoLingua ) {
            $isoLingua = config( 'locale' );
        }
        // Se non setto una chiave restituisco tutti i meta
        if ( ! $key ) {
            return $this->hasMany( 'Plugins\ECOMMERCE\Models\CategoryMeta', 'id_category', 'id' )->where( 'iso', $isoLingua );
        }
        // se ho la chiave restituisco il valore di quel meta in quella lingua
        $val =  $this->hasMany( 'Plugins\ECOMMERCE\Models\CategoryMeta', 'id_category', 'id' )
            ->where( 'meta_key', $key )
            ->where( 'iso', $isoLingua )
            ->first();
        if($val) return $val->value;
        else return false;
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return Category::create($item);
        } else {
            Category::where('id', '=', $item['id'])->update($item);
            return Category::find($item['id']);
        }
    }

    public function parent() {
        if($this->id_parent){
            return Category::find($this->id_parent);
        } else return null;
    }

    public function categoryRoot() {
        $categ = $this;
        /*for($i=0;$i<5;$i++){
            if($categ){
                if(($categ->id_parent) && $categ->id_parent>0)
                    $categ = Category::find($categ->id_parent);
                else
                    break;
            }
        }*/
        $pathCat = $categ->path;
        $arrayCat = explode("|",$pathCat );
        if($arrayCat)
            return  Category::find($arrayCat[0]);
        else return null;

    }

}