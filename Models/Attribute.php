<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 15/07/2018
 * Time: 10:24
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_attribute';
    protected $fillable = array('title','status','id_feature');


    public function feature() {
        return $this->hasOne( 'Plugins\ECOMMERCE\Models\Feature', 'id', 'id_feature' );
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            return Attribute::create($item);
        } else {
            Attribute::where('id', '=', $item['id'])->update($item);
            return Attribute::find($item['id']);
        }
    }

}