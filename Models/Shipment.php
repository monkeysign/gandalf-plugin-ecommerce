<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 02/06/2018
 * Time: 21:26
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;


class Shipment extends Eloquent {

    use SoftDeletes;

    protected $dates = [ 'deleted_at' ];
    protected $table = 'ecommerce_shipment';
    protected $fillable = array( 'title','status','code','description', 'price', 'free');



    /**
     * Salva un nuovo record o fa l'update
     * @param $item
     *
     * @return \Plugins\CMS\Models\Post
     */
    public static function saveOrUpdate( $item ) {
        if ( ! isset( $item['id'] ) || ! $item['id'] ) {
            return Shipment::create( $item );
        } else {
            Shipment::where( 'id', $item['id'] )->update( $item );
            return Shipment::find( $item['id'] );
        }
    }
}
