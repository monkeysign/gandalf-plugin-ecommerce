<?php

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CategoryMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_category_meta';
    protected $fillable = array('id_category', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function category() {
        return $this->belongsTo('Plugins\ECOMMERCE\Models\Category', 'id', 'id_category');
    }

}
