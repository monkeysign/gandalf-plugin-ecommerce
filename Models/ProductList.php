<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 15/07/2018
 * Time: 18:38
 */

namespace Plugins\ECOMMERCE\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductList extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ecommerce_product';
    protected $fillable = array('title','status','price','id_category','state_product','ean','permalink','path_label');


}
