<?php

namespace Plugins\ECOMMERCE\Controllers;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Modules\Backend\Classes\Controller;
use Plugins\ECOMMERCE\Models\Brand;
use Plugins\ECOMMERCE\Models\Category;
use Plugins\ECOMMERCE\Models\Feature;
use Plugins\ECOMMERCE\Models\Product;
use Plugins\ECOMMERCE\Models\ProductList;
use Plugins\ECOMMERCE\Models\ProductMeta;
use MCS;

class Products extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        //$this->param['table'] = Product::orderBy('id', 'DESC')->orderBy('id', 'desc')->limit(500)->get();
        return view()->render('ecommerce.product.list', array());
    }

    public function listAllAjax() {


        $query = ProductList::
        leftJoin('ecommerce_category', 'ecommerce_category.id', '=', 'ecommerce_product.id_category')
            ->leftJoin('ecommerce_product_type', 'ecommerce_category.id_product_type', '=', 'ecommerce_product_type.id')
            ->select('ecommerce_product.*', 'ecommerce_category.path_label as path_label', 'ecommerce_product_type.title as titleP');

        //gestisco le where
        $arrayColonne = array("checkbox", "ecommerce_product.ean", "ecommerce_product.title", "ecommerce_category.path_label", "ecommerce_product.permalink", "ecommerce_product.quantity", "ecommerce_product.price", "ecommerce_product_type.title", "ecommerce_product.rilevance", "ecommerce_product.state_product");

        //verifico la presenza del'informazioni generale
        if((request()->get('sSearch')) and strlen(request()->get('sSearch')) > 0) {
            $query = $query->where($arrayColonne[1], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[2], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[3], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[4], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[7], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[8], 'like', '%' . request()->get('sSearch') . '%');
            $query = $query->orWhere($arrayColonne[9], 'like', '%' . request()->get('sSearch') . '%');
        } else {

            for($i = 0; $i < count($arrayColonne); $i++) {
                if((request()->get('sSearch_' . $i)) and strlen(request()->get('sSearch_' . $i)) > 0) {
                    if($i < 5) {
                        $query = $query->where($arrayColonne[$i], 'like', '%' . request()->get('sSearch_' . $i) . '%');
                    } else if($i == 5) { //Status Quantita
                        $query = $query->where($arrayColonne[$i], '=', request()->get('sSearch_' . $i));
                    } else if($i == 6) { //Status prezzo
                        $query = $query->where($arrayColonne[$i], 'like', (request()->get('sSearch_' . $i)));
                    } else if($i == 7) { //Status prodotto
                        $query = $query->where($arrayColonne[$i], '=', request()->get('sSearch_' . $i));
                    } else if($i == 8) { //Status prodotto
                        $query = $query->where($arrayColonne[$i], '=', request()->get('sSearch_' . $i));
                    } else if($i == 9) { //Status prodotto
                        if(strtolower(request()->get('sSearch_' . $i)) == 'nuovo') {
                            $query = $query->where($arrayColonne[$i], '=', 0);
                        } else if(strtolower(request()->get('sSearch_' . $i)) == 'usato') {
                            $query = $query->where($arrayColonne[$i], '=', 1);
                        } else if(strtolower(request()->get('sSearch_' . $i)) == 'in arrivo') {
                            $query = $query->where($arrayColonne[$i], '=', 2);
                        } else if(strtolower(request()->get('sSearch_' . $i)) == 'in rientro') {
                            $query = $query->where($arrayColonne[$i], '=', 3);
                        }
                    }
                }
            }
        }


        $iTotal = $query->count();
        //limit
        $sLimit = "";
        $displayStart = request()->get('iDisplayStart');
        $displayLength = request()->get('iDisplayLength');

        if(isset($displayStart) && $displayLength != '-1') {
            $query = $query->skip($displayStart)->take($displayLength);
        }
        $aResultFilterTotal = $query->orderBy('ecommerce_product.id', 'desc')->get();

        $sOutput = '{';
        $sOutput .= '"sEcho": ' . intval($_GET['sEcho']) . ', ';
        $sOutput .= '"iTotalRecords": ' . $iTotal . ', ';
        $sOutput .= '"iTotalDisplayRecords": ' . $iTotal . ', ';
        $sOutput .= '"aaData": [ ';
        foreach($aResultFilterTotal as $aRow) {

            $linkUpdate = path_for('admin.ecommerce.product.update', ['id' => $aRow->id]);
            $linkDelete = path_for('admin.ecommerce.product.delete', ['id' => $aRow->id]);

            $azioni = <<<BOT
<a href="$linkUpdate" class="btn btn-sm btn-default"><span class="fa fa-pencil"></span></a><a href="#" data-path="$linkDelete" class="btn btn-sm btn-danger delete-item"><span class="fa fa-trash-o"></span></a>
BOT;

            $checkbox = <<<BOT
<input type="checkbox" name="massive" value="{$aRow->id}" />
BOT;

            //
            switch($aRow->status) {
                case 2:
                    $aRow->status = 'Disabilitato';
                    break;
                case 1:
                    $aRow->status = 'Abilitato';
                    break;
            }

            switch($aRow->state_product) {
                case 0:
                    $aRow->state_product = 'Nuovo';
                    break;
                case 1:
                    $aRow->state_product = 'Usato';
                    break;
                case 2:
                    $aRow->state_product = 'In Arrivo';
                    break;
                case 3:
                    $aRow->state_product = 'In Rientro';
                    break;
            }

            $sOutput .= "[";
            $sOutput .= '"' . addslashes($checkbox) . '",';
            $sOutput .= '"<span class=\"bold-title-table\">' . $aRow->ean . '<\/span>",';
            $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", addslashes($aRow->title)) . '<\/span>",';
            $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", $aRow->path_label) . '<\/span>",';
            $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", $aRow->permalink) . '<\/span>",';

            $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", $aRow->quantity) . '<\/span>",';
            if($aRow->price_offer > 0) {
                $sOutput .= '"<span class=\"bold-title-table\"><small style=\"color:#9f9f9f; text-decoration: line-through\">' . str_replace("\'", "'", $aRow->price) . '<\/small> ' . str_replace("\'", "'", $aRow->price_offer) . '<\/span>",';
            } else {
                $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", $aRow->price) . '<\/span>",';
            }
            $sOutput .= '"<span class=\"bold-title-table\">' . str_replace("\'", "'", $aRow->titleP) . '<\/span>",';

            $sOutput .= '"<span class=\"bold-title-table\">' . $aRow->rilevance . '<\/span>",';
            $sOutput .= '"<span class=\"bold-title-table\">' . $aRow->state_product . '</span>",';
            $sOutput .= '"' . addslashes($azioni) . '"';
            $sOutput .= "],";

        }
        $sOutput = substr_replace($sOutput, "", -1);
        $sOutput .= '] }';

        echo $sOutput;
    }


    /**
     * Upload image for gallery
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function galleryUpload() {
        if(!is_dir(config('media') . '/ecommerce/prodotti')) {
            mkdir(config('media') . '/ecommerce/prodotti', 0777, true);
        }
        foreach($_FILES as $key => $file) {
            $file["name"] = date('U') . '_' . $file["name"];
            if(!move_uploaded_file($file['tmp_name'], config('media') . '/ecommerce/prodotti/' . $file['name'])) {
                $param = [
                    'state' => false,
                ];
            } else {
                $param = [
                    'state' => true,
                    'file' => config('httpmedia') . '/ecommerce/prodotti/' . $file['name'],
                    'filename' => $file['name'],
                ];
            }

            return $param;
        }
    }

    /**
     * Delete a file
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function galleryDelete() {
        $items = request()->get('filename');
        unlink(config('media') . '/ecommerce/prodotti/' . $items);
    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form($id = null) {
        $param['allBrands'] = Brand::all();
        $param['allCategories'] = Category::all();
        $param['features'] = [];
        $param['attribute'] = [];
        $param['attributeS'] = [];
        $param['catRoot'] = Category::where('level', '=', '0')->orderBy('orders', 'ASC')->get();
        $param['arrayCategorySec'] = array();
        $param['categorySecond'] = '';

        $param['compatibilita'] = Feature::where('status', '=', '3')->orderBy('id', 'ASC')->get();

        $allIds = substr(request()->get('allIds'), 0, -1);
        $param['allIds'] = $allIds;

        if($allIds) {
            $features = Feature::whereIn('id', [92, 150, 188, 190, 209])->get();
            $feature_attribute[] = array();
            if($features) {
                foreach($features as $feature) {
                    $feature_attribute[$feature->id] = $feature->attributes;
                }
            }
            $param['features'] = $features;
            $param['attribute'] = $feature_attribute;
        }


        if(isset($id) && $id) {
            $param['record'] = Product::find($id);
            $arraySel = [];
            foreach($param['record']->attributes as $attr) {
                $arraySel[] = $attr->id;
            }
            if($param['record']->id_category > 0) {
                $category = Category::find($param['record']->id_category);
                $features = $category->productType->features;
                $feature_attribute[] = array();
                if($features) {
                    foreach($features as $feature) {
                        $feature_attribute[$feature->id] = $feature->attributes;
                    }
                }
                $param['features'] = $features;
                $param['attribute'] = $feature_attribute;


                $categorySec = $param['record']->categorySec;
                $stringaCategorySec = '';
                $arrayCategorySec = array();
                if($categorySec) {
                    foreach($categorySec as $category) {
                        $stringaCategorySec .= $category->id . ',';
                        $arrayCategorySec[] = $category->id;
                    }
                    $stringaCategorySec = substr($stringaCategorySec, 0, strlen($stringaCategorySec) - 1);
                    $param['categorySecond'] = $stringaCategorySec;
                    $param['arrayCategorySec'] = $arrayCategorySec;
                }


            }
            $param['attributeS'] = $arraySel;
        } else {
            $param['record'] = new Product();
        }

        return view()->render('ecommerce.product.form', $param);
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get('item');
        $allIds = request()->get('allIds');

        try {
            if($allIds) {
                //massiva
                $allIds = explode(',', $allIds);
                $item['note'] = (trim(strip_tags($item['note'])) == '') ? '' : $item['note'];
                $item['description'] = (trim(strip_tags($item['description'])) == '') ? '' : $item['description'];
                $item = array_filter($item, function($value) {
                    return $value !== '';
                });
                foreach($allIds as $id) {
                    $item['id'] = $id;
                    $record = Product::saveOrUpdate($item);
                    $attribute = request()->get('feature');
                    $arrayAttr = array();
                    if($attribute) {
                        foreach($attribute as $attr) {
                            if($attr) {
                                $arrayAttr[] = $attr;
                            }
                        }
                    }
                    $compatibilita = request()->get('compatibilita');
                    if($compatibilita) {
                        foreach($compatibilita as $comp) {
                            if($comp) {
                                $arrayAttr[] = $comp;
                            }
                        }
                    }

                    //save meta
                    $this->saveProductMeta($record, $allIds);

                    if(count($arrayAttr)) {
                        foreach($arrayAttr as $sAttr) {
                            $record->attributes()->detach($sAttr);
                            $record->attributes()->attach($sAttr);
                        }

                    }

                    $catsec = request()->get('catsec');
                    $arrayCatSecond = array();
                    if($catsec) {
                        $arrayCatSec = explode(",", $catsec);

                        foreach($arrayCatSec as $cats) {
                            $arrayCatSecond[] = $cats;
                        }
                        $record->categorySec()->detach();
                        $record->categorySec()->attach($arrayCatSecond);
                    }
                }
                $param = [
                    'record' => [],
                    'state' => true,
                    'mex' => 'Salvataggio Riuscito',
                ];

            } else {
                //singola
                if(!isset($item['permalink']) || strlen($item['permalink']) == 0) {
                    $item['permalink'] = str_replace(' ', '-', strtolower(trim(preg_replace("/[^a-zA-Z0-9 ]/", '', $item['title']))));
                }
                $record = Product::saveOrUpdate($item);
                $attribute = request()->get('feature');
                $arrayAttr = array();
                if($attribute) {
                    foreach($attribute as $attr) {
                        if($attr) {
                            $arrayAttr[] = $attr;
                        }
                    }
                }


                $compatibilita = request()->get('compatibilita');
                if($compatibilita) {
                    foreach($compatibilita as $comp) {
                        if($comp) {
                            $arrayAttr[] = $comp;
                        }
                    }
                }


                //save meta
                $this->saveProductMeta($record);

                $record->attributes()->detach();
                $record->attributes()->attach($arrayAttr);

                $catsec = request()->get('catsec');
                $arrayCatSecond = array();
                if($catsec) {
                    $arrayCatSec = explode(",", $catsec);

                    foreach($arrayCatSec as $cats) {
                        $arrayCatSecond[] = $cats;
                    }
                }
                $record->categorySec()->detach();
                $record->categorySec()->attach($arrayCatSecond);

                if($record->sku && $record->amazon) {
                    $client = new MCS\MWSClient([
                        'Marketplace_Id' => 'APJ6JRA9NG5V4',
                        'Seller_Id' => 'A11GV99221YON6',
                        'Access_Key_ID' => 'AKIAJQZB7JCGOV5JW3LA',
                        'Secret_Access_Key' => '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'
                        //            'MWSAuthToken' => '' // Optional. Only use this key if you are a third party user/developer
                    ]);

                    // Optionally check if the supplied credentials are valid
                    try {
                        $product = new MCS\MWSProduct();
                        $product->sku = $record->sku;
                        $product->price = ($record->offer) ? $record->price_offer + 3 : $record->price + 3;
                        $product->product_id = $record->ean;
                        $product->product_id_type = 'EAN';
                        $product->quantity = $record->quantity;
                        $product->condition_type = $record->state_product == 1 ? 'UsedVeryGood' : 'New';
                        if($record->state_product == 1) {
                            $product->condition_note = "Prodotto in ottime condizioni gia' utilizzato da precedente possessore. I prodotti usati non includono contenuti aggiuntivi e DLC perche' gia' utilizzati";
                        }

                        $asin = null;

                        // cerco l'asin della versione standard
                        $infoWMS = $client->ListMatchingProducts($record->ean);
                        if(isset($infoWMS['Products']['Product']) && count($infoWMS['Products']['Product']) > 1) {
                            foreach($infoWMS['Products']['Product'] as $productWMS) {
                                if(isset($productWMS['AttributeSets']['ItemAttributes']['Edition']) && strtolower($productWMS['AttributeSets']['ItemAttributes']['Edition']) == 'standard') {
                                    $asin = $productWMS['Identifiers']['MarketplaceASIN']['ASIN'];
                                }
                            }
                        }

                        // se non ci sta la standard cerco la day-one
                        if(!$asin) {
                            if(isset($infoWMS['Products']['Product']) && count($infoWMS['Products']['Product']) > 1) {
                                foreach($infoWMS['Products']['Product'] as $productWMS) {
                                    if(isset($productWMS['AttributeSets']['ItemAttributes']['Edition']) && strtolower($productWMS['AttributeSets']['ItemAttributes']['Edition']) == 'day-one') {
                                        $asin = $productWMS['Identifiers']['MarketplaceASIN']['ASIN'];
                                    }
                                }
                            }
                        }

                        if($asin) {
                            $product->product_id = $asin;
                            $product->product_id_type = 'ASIN';
                        }

                        if($product->validate()) {
                            $client->postProduct($product);

                        } else {
                            $errorMWS = $product->getValidationErrors();

                            return [
                                'record' => $record,
                                'state' => false,
                                'mex' => $errorMWS['product_id'],
                            ];
                        }

                    } catch(\Exception $e) {
                        return [
                            'record' => [],
                            'state' => false,
                            //'mex' => "Non posso connettermi ad Amazon"
                            'mex' => 'Errore Amazon: ' . $e->getMessage()
                        ];
                    }
                }
                $param = [
                    'record' => $record,
                    'state' => true,
                    'mex' => 'Salvataggio Riuscito',
                ];
            }
        } catch(\Ring\Exception\ValidationException $ex) {
            die($ex->getMessage());
        }

        return $param;
    }

    /**
     * Salva i meta del post
     *
     * @param Post $post
     */
    public function saveProductMeta(\Plugins\ECOMMERCE\Models\Product $product, $allIds = '') {
        $items = request()->get('meta');

        if($allIds) {
            $items['techincalnote'] = (trim(strip_tags($items['techincalnote'])) == '') ? '' : $items['techincalnote'];
            $items['boxcontent'] = (trim(strip_tags($items['boxcontent'])) == '') ? '' : $items['boxcontent'];
            unset($items['imghighlight']);
            unset($items['gallery']);
            $items = array_filter($items);
        }

        if(request()->get('language') && request()->get('language') !== '') {
            $iso = request()->get('language');
        } else {
            $iso = config('locale');
        }

        // Immagini
        if(!is_dir(config('media') . '/ecommerce/prodotti')) {
            mkdir(config('media') . '/ecommerce/prodotti', 0777, true);
        }
        foreach($_FILES as $key => $file) {
            if($key === 'files') {
                continue;
            }
            if($file['name']) {
                $file["name"] = date('U') . '_' . $file["name"];
                if(!move_uploaded_file($file['tmp_name'], config('media') . '/ecommerce/prodotti/' . $file['name'])) {
                    echo 'error file upload';
                }
                $oldValue = ProductMeta::where('id_product', $product->id)->where('iso', $iso)->where('meta_key', $key)->first();
                if($oldValue) {
                    unlink(config('media') . '/ecommerce/prodotti/' . $oldValue->value);
                    ProductMeta::where('id_product', $product->id)->where('iso', $iso)->where('meta_key', $key)->delete();
                }
                $metaArray['meta_key'] = $key;
                $metaArray['value'] = $file['name'];
                $metaArray['id_product'] = $product->id;
                $metaArray['iso'] = $iso;
                ProductMeta::create($metaArray);
            } else {
                if(isset($_POST[$key . '-old']) && $_POST[$key . '-old'] == '') {
                    $oldValue = ProductMeta::where('id_product', $product->id)->where('iso', $iso)->where('meta_key', $key)->first();
                    if($oldValue) {
                        if(file_exists(config('media') . '/ecommerce/prodotti/' . $oldValue->value)) {
                            unlink(config('media') . '/ecommerce/prodotti/' . $oldValue->value);
                        }
                        ProductMeta::where('id_product', $product->id)->where('iso', $iso)->where('meta_key', $key)->delete();
                    }
                }
            }
        }

        // altri meta
        foreach($items as $key => $value) {
            // Cancello le tassonomie associate
            ProductMeta::where('id_product', '=', $product->id)->where('iso', $iso)->where('meta_key', $key)->delete();

            $metaArray['meta_key'] = $key;
            if(is_array($items[$key])) {
                $metaArray['value'] = serialize($value);
            } else {
                $metaArray['value'] = $value;
            }
            $metaArray['id_product'] = $product->id;
            $metaArray['iso'] = $iso;
            ProductMeta::create($metaArray);
        }
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete($id = null) {
        $record = Product::find($id);
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array('result' => true);

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get('ids');
        Product::whereIn('id', $group)->delete();
        $data = array('result' => true);

        return $data;
    }

    public function eanSearch() {
        $ean = request()->get('code');
        $products = Product::where('ean', $ean)->orWhere('ean2', $ean)->get();

        return json_encode($products);
    }

    public function scarico() {
        $id = request()->get('id');
        $products = Product::find($id);
        $products->quantity--;
        $products->update();

        return json_encode(["result" => true]);
    }
}