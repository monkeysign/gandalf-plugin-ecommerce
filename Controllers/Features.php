<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 07/07/2018
 * Time: 11:00
 */

namespace Plugins\ECOMMERCE\Controllers;


use Plugins\ECOMMERCE\Models\Category;
use Plugins\ECOMMERCE\Models\Feature;
use Modules\Backend\Classes\Controller;

class Features extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        $this->param['table'] = Feature::orderBy( 'id', 'DESC' )->get();
        return view()->render( 'ecommerce.feature.list', $this->param );
    }



    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        if ( isset( $id ) && $id ) {
            $param['record'] = Feature::find( $id );
        } else {
            $param['record'] = new Feature();
        }

        return view()->render( 'ecommerce.feature.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get( 'item' );
        try {
            $record = Feature::saveOrUpdate( $item );
            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        return $param;
    }








    /**
     * Ajax feature option
     */
    public function ajaxFeature(){
        $category = Category::find( request()->get( 'idCategory' ) );
        $features = $category->features;
        $html= "<div class='row'>";
        if($features){
            foreach ($features as $feature){
                $html.= "<div class=\"col-md-4\">
                        <div class=\"form-group\">
                        <label title=\"Campo \" class=\"control-label\">".$feature->title."</label>
                        <div class=\"input-group\">
                        <div class=\"input-group-addon\">
                        <i class=\"ti-user\"></i>
                        </div>
                        <select name=\"feature[feature_".$feature->id."]\" data-placeholder=\"Seleziona la categoria\" class=\"select2 select-choose form-control\" id=\"select-category\">
                        <option value=\"\">Seleziona un valore</option>";
                $attributes = $feature->attributes;
                if($attributes){
                    foreach ($attributes as $attribute) {
                        $html.= "<option value='".$attribute->id."'>".$attribute->title."</option>";
                    }
                }

                $html.= "</select>
                        </div>
                        <span class=\"help-block with-errors\"> </span>
                        </div>
                        </div>";
            }
            $html.= "</div>";
        }
        return $html;


    }
    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete( $id = null ) {
        $record = Feature::find( $id );
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array( 'result' => true );

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get( 'ids' );
        Feature::whereIn( 'id', $group )->delete();
        $data = array( 'result' => true );

        return $data;
    }
}