<?php
namespace Plugins\ECOMMERCE\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\ECOMMERCE\Models\Category;
use Plugins\ECOMMERCE\Models\CategoryMeta;
use Plugins\ECOMMERCE\Models\Feature;
use Plugins\ECOMMERCE\Models\ProductType;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


class Categories extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        //$this->param['table'] = Category::orderBy('path_label', 'ASC')->get();
        $this->param['catRoot'] = Category::where('level', '=','0' )->orderBy('orders', 'ASC')->get();
        return view()->render( 'ecommerce.category.list', $this->param );
    }

    /**
     * Genero il json per la lista
     * @return array
     * @throws \Exception
     */
    public function recordList() {
        $record    = new Category();
        $dataTable = hooks()->apply_filters( ECOMMERCE_CATEGORY_LIST, $record );
        return $dataTable->make();
    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        $param['allType']= ProductType::orderBy( 'id', 'desc' )->get();
        $param['catRoot'] = Category::where('level', '=','0' )->orderBy('orders', 'ASC')->get();
        $disabledParent= false;
        if ( isset( $id ) && $id ) {
            $param['record'] = Category::find( $id );
            $param['allCat'] = Category::where('id', '<>', $id)->orderBy('orders', 'ASC')->get();

            $categoriaFiglia =  Category::where('id_parent', '=', $id)->first();
            if($categoriaFiglia){
                $disabledParent= true;
            }
        } else {
            $param['record'] = new Category();
            $param['allCat'] = Category::all();
        }

        $param['disabledParent'] = $disabledParent;
        return view()->render( 'ecommerce.category.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get( 'item' );
        if ( request()->get( 'language' ) && request()->get( 'language' ) !== '' ) {
            $iso = request()->get( 'language' );
        } else {
            $iso = config( 'locale' );
        }

        $item['level']=0;

        try {
            //Gestisco il livello della categoria
            if(isset($item['id_parent']) && $item['id_parent'] > 0){
                $categoriaParent= Category::where( 'id', '=', $item['id_parent'])->first();
                if($categoriaParent)
                    $item['level']= $categoriaParent->level + 1;
                else
                    $item['level']= 0;
            }

            $record = Category::saveOrUpdate( $item );

            if(isset($item['id']) && $item['id']>0){
                $categoriaCorrente = Category::where( 'id', '=', $item['id'])->first();
                //Aggiorno il livello dei discendenti del nodo corrente
                $differenzaProfondita= $item['level']-$categoriaCorrente->level;
                $discendenti = Category::where( 'path', 'like', "%". $categoriaCorrente->path.' |%')->get();
                if($discendenti){
                    foreach ($discendenti as $figli){
                        $figli->level= $figli->level+($differenzaProfondita);
                        $figli->save();
                    }
                }
            }

            if($item['id_product_type'] === '')  unset($item['id_product_type']);

            $meta = request()->get( 'meta' );

            // Immagini
            if(!is_dir(config( 'media' ) . '/ecommerce/categorie')){
                mkdir(config( 'media' ) . '/ecommerce/categorie', 0777, true);
            }
            foreach ( $_FILES as $key => $file ) {
                if ( $key === 'files' ) {
                    continue;
                }
                if ( $file['name'] ) {
                    $file["name"] = date( 'U' ) . '_' . $file["name"];
                    if ( ! move_uploaded_file( $file['tmp_name'], config( 'media' ) .  '/ecommerce/categorie/' . $file['name'] ) ) {
                        echo 'error file upload';
                    }
                    $oldValue = CategoryMeta::where( 'id_category', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
                    if ( $oldValue ) {
                        unlink( config( 'media' ) . '/ecommerce/categorie/' . $oldValue->value );
                        CategoryMeta::where( 'id_category', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
                    }
                    $metaArray['meta_key'] = $key;
                    $metaArray['value']    = $file['name'];
                    $metaArray['id_category']  = $record->id;
                    $metaArray['iso']      = $iso;
                    CategoryMeta::create( $metaArray );
                } else {
                    if ( isset( $_POST[ $key . '-old' ] ) && $_POST[ $key . '-old' ] == '' ) {
                        $oldValue = CategoryMeta::where( 'id_category', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
                        if ( $oldValue ) {
                            if ( file_exists( config( 'media' ) .  '/ecommerce/categorie/' . $oldValue->value ) ) {
                                unlink( config( 'media' ) .  '/ecommerce/categorie/' . $oldValue->value );
                            }
                            CategoryMeta::where( 'id_category', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
                        }
                    }
                }
            }


            // altri meta
            foreach ( $meta as $key => $value ) {
                // Cancello le tassonomie associate
                CategoryMeta::where( 'id_category', '=', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();

                $metaArray['meta_key'] = $key;
                if ( is_array( $meta[ $key ] ) ) {
                    $metaArray['value'] = serialize( $value );
                } else {
                    $metaArray['value'] = $value;
                }
                $metaArray['id_category'] = $record->id;
                $metaArray['iso']     = $iso;
                CategoryMeta::create( $metaArray );
            }


            $this->updatePath();

            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        return $param;
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete( $id = null ) {
        $record = Category::find( $id );
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array( 'result' => true );

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get( 'ids' );
        Category::whereIn( 'id', $group )->delete();
        $data = array( 'result' => true );

        return $data;
    }



    public function updatePath(){
        $listCategorie = Category::orderBy('level', 'ASC')->get();
        if($listCategorie){
            foreach ($listCategorie as $categ){
                if($categ->level==0){
                    $categ->path=$categ->id;
                    $categ->path_label = $categ->title;
                    $categ->permalink = $this->createPermalink($categ->title);
                }else{
                    if($categ->categoryFather){
                        $categ->path=$categ->categoryFather->path .' | '.$categ->id;
                        $categ->path_label =$categ->categoryFather->path_label .' | '.$categ->title;
                        $categ->permalink = $categ->categoryFather->permalink.'/'.$this->createPermalink($categ->title);
                    }else{
                        $categ->path=$categ->id;
                        $categ->path_label =$categ->title;
                        $categ->permalink = $this->createPermalink($categ->title);
                    }
                }
                $categ->save();
            }
        }
    }


    public function createPermalink($stringa){
        return str_replace(' ', '-', strtolower(trim(preg_replace("/[^a-zA-Z0-9 ]/", '', $stringa))));
    }


}