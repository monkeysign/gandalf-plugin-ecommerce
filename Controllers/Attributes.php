<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 07/07/2018
 * Time: 11:00
 */

namespace Plugins\ECOMMERCE\Controllers;


use Plugins\ECOMMERCE\Models\Attribute;
use Modules\Backend\Classes\Controller;
use Plugins\ECOMMERCE\Models\Feature;

class Attributes extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        $this->param['table'] = Attribute::orderBy( 'id', 'DESC' )->get();
        return view()->render( 'ecommerce.attribute.list', $this->param );
    }



    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        $param['allFeatures'] = Feature::all();
        if ( isset( $id ) && $id ) {
            $param['record'] = Attribute::find( $id );
        } else {
            $param['record'] = new Attribute();
        }

        return view()->render( 'ecommerce.attribute.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get( 'item' );
        try {
            $record = Attribute::saveOrUpdate( $item );
            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        return $param;
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete( $id = null ) {
        $record = Attribute::find( $id );
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array( 'result' => true );

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get( 'ids' );
        Attribute::whereIn( 'id', $group )->delete();
        $data = array( 'result' => true );

        return $data;
    }
}