<?php

namespace Plugins\ECOMMERCE\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CRM\Customer\Models\Customer as Customer;
use Plugins\CRM\CustomerCare\Models\Practice as Practice;
use Plugins\CRM\CustomerCare\Models\TypeOp;
use Plugins\ECOMMERCE\Models\Brand;
use Plugins\ECOMMERCE\Models\BrandMeta;

class Brands extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        $this->param['table'] = Brand::orderBy( 'id', 'DESC' )->get();
        return view()->render( 'ecommerce.brand.list', $this->param );
    }

    /**
     * Genero il json per la lista
     * @return array
     * @throws \Exception
     */
    public function recordList() {
        $record    = new Brands();
        $dataTable = hooks()->apply_filters( CRM_ADMIN_ECOMMERCE_BRAND_LIST, $record );
        return $dataTable->make();
    }


    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        if ( isset( $id ) && $id ) {
            $param['record'] = Brand::find( $id );
        } else {
            $param['record'] = new Brand();
        }

        return view()->render( 'ecommerce.brand.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get( 'item' );
        $meta = request()->get( 'meta' );
        if(!$meta) $meta = array();

        if ( request()->get( 'language' ) && request()->get( 'language' ) !== '' ) {
            $iso = request()->get( 'language' );
        } else {
            $iso = config( 'locale' );
        }
        
        try {
            $record = Brand::saveOrUpdate( $item );

            // Immagini
            if(!is_dir(config( 'media' ) . '/ecommerce/brand')){
                mkdir(config( 'media' ) . '/ecommerce/brand', 0777, true);
            }
            foreach ( $_FILES as $key => $file ) {
                if ( $key === 'files' ) {
                    continue;
                }
                if ( $file['name'] ) {
                    $file["name"] = date( 'U' ) . '_' . $file["name"];
                    if ( ! move_uploaded_file( $file['tmp_name'], config( 'media' ) .  '/ecommerce/brand/' . $file['name'] ) ) {
                        echo 'error file upload';
                    }
                    $oldValue = BrandMeta::where( 'id_brand', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
                    if ( $oldValue ) {
                        unlink( config( 'media' ) . '/ecommerce/brand/' . $oldValue->value );
                        BrandMeta::where( 'id_brand', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
                    }
                    $metaArray['meta_key'] = $key;
                    $metaArray['value']    = $file['name'];
                    $metaArray['id_brand']  = $record->id;
                    $metaArray['iso']      = $iso;
                    BrandMeta::create( $metaArray );
                } else {
                    if ( isset( $_POST[ $key . '-old' ] ) && $_POST[ $key . '-old' ] == '' ) {
                        $oldValue = BrandMeta::where( 'id_brand', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
                        if ( $oldValue ) {
                            if ( file_exists( config( 'media' ) .  '/ecommerce/brand/' . $oldValue->value ) ) {
                                unlink( config( 'media' ) .  '/ecommerce/brand/' . $oldValue->value );
                            }
                            BrandMeta::where( 'id_brand', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
                        }
                    }
                }
            }

            // altri meta
            foreach ( $meta as $key => $value ) {
                // Cancello le tassonomie associate
                BrandMeta::where( 'id_brand', '=', $record->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();

                $metaArray['meta_key'] = $key;
                if ( is_array( $meta[ $key ] ) ) {
                    $metaArray['value'] = serialize( $value );
                } else {
                    $metaArray['value'] = $value;
                }
                $metaArray['id_brand'] = $record->id;
                $metaArray['iso']     = $iso;
                BrandMeta::create( $metaArray );
            }
            
            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        return $param;
    }

}