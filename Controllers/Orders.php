<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 07/08/2018
 * Time: 20:32
 */

namespace Plugins\ECOMMERCE\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CRM\Customer\Models\Customer;
use Plugins\ECOMMERCE\Models\Order;
use Plugins\ECOMMERCE\Models\OrderList;

class Orders extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        $this->param['table']  = OrderList::leftJoin('customer', 'customer.id', '=', 'ecommerce_order.id_customer')
            ->select('ecommerce_order.*', 'customer.name as name' , 'customer.surname as surname')->orderBy('id', 'DESC')->get();
        return view()->render( 'ecommerce.order.list', $this->param );

    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        if ( isset( $id ) && $id ) {
            $order= Order::find( $id );
            $this->formatPrice($order);
            $param['record'] = $order;
        } else {
            $param['record'] = new Order();
        }

        return view()->render( 'ecommerce.order.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {

        $item = request()->get( 'item' );
        $order = Order::find( $item['id'] );
        if($item['payment_date']=='')
            unset($item['payment_date']);
        try {
            $record = Order::saveOrUpdate( $item );
            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        if($item['status']==2 || $item['status']==4 || $item['status']==5 || $item['status']==6 ){
            if($item['status'] != request()->get( 'status_old' )){
                hooks()->do_action( CRM_ORDER_STATUS_SEND_MAIL, $record );
            }
        }


        return $param;
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete( $id = null ) {
        $record = Order::find( $id );
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array( 'result' => true );

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get( 'ids' );
        Order::whereIn( 'id', $group )->delete();
        $data = array( 'result' => true );

        return $data;
    }


    public function formatPrice($record)
    {
        if (isset($record->total) && is_numeric($record->total))
            $record->total_label = number_format($record->total, 2, ',', ' ');

    }


    public function pdfOrdine($id = null )
    {
        $ret=null;
        $nome = "Stampa-ordine.pdf";
        $pdf = container()->get('pdf');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);

        if ( isset( $id ) && $id ) {
            $order= Order::find( $id );
            $this->intestazioneSinistraPDF($pdf,$order);
            $this->intestazioneDestraPDF($pdf, $order);
            $this->addTabellaInformazioni($pdf, $order);

            $cols = array("CODICE" => 20,
                "DESCRIZIONE" => 85,
                "U.M." => 25,
                "Q.TA" => 25,
                "PREZZO Un." => 35);
            $pdf->addCols($cols);

            $pdf->addLineFormat($cols);

            $y = 80;
            $prezzoTotaleNetto=0;
            $totalePrezzoSenzaSconto = 0;
            foreach ($order->orderProduct as $prodotto) {

                $line = array("CODICE" => " $prodotto->id_product",
                    "DESCRIZIONE" => " $prodotto->title",
                    "U.M." => "PZ",
                    "Q.TA" => " " . $prodotto->quantity,
                    "PREZZO Un." => " " . $this->formatPrezzoForModule($prodotto->price)

                );
                $size = $pdf->addLine($y, $line);
                $y += $size + 2;

                $prezzoTotaleNetto+=$prodotto->price*$prodotto->quantity;
                $totalePrezzoSenzaSconto+=  $prodotto->price*$prodotto->quantity;
            }

            $labelSpedizione=$this->formatPrezzoForModule($order->cost_ship);


            $line = array("CODICE" => " ",
                "DESCRIZIONE" => $order->typeShip->title ,
                "U.M." => " ",
                "Q.TA" => " 1 ",
                "PREZZO Un." => "$labelSpedizione"
            );
            $size = $pdf->addLine($y, $line);

            $pdf->addTabellaFinaleInfo($order->total, "",$order->total,$order->total,$order->cost_ship,"");





        }




        //$ret= $pdf->Output('D',$nome,true);
        $ret= $pdf->Output($nome,'D');
        return $ret;

    }

    public function formatPrezzoForModule($prezzo) {
        return number_format($prezzo, 2);
    }

    public function intestazioneSinistraPDF($pdf,$order) {
        $riga = " via Alcide De Gasperi 215/217 a Pagani (SA) \n";
        //$riga.= "Tel 345 1696794 \n";
        $riga.= "info@blobvideo.it \n";
        //$riga.= "P.IVA 05474180659 \n";
        //Positionnement en bas
        $x1 = 10;
        $y1 = 20;
        $pdf->SetXY($x1, $y1);
        $pdf->SetFont('Arial', 'B', 10);
        $length = $pdf->GetStringWidth(" BlobVideo.com");
        $pdf->Cell($length, 2, ' BlobVideo.com');
        $pdf->SetXY($x1, $y1 + 4);
        $pdf->SetFont('Arial', '', 8);
        $length = $pdf->GetStringWidth($riga);
        //Coordonnées de la société
        //$lignes = $pdf->sizeOfText($riga, $length);
        $pdf->MultiCell($length, 4, $riga);

        $x1 = 10;
        $y1 = 40;
        $pdf->SetXY($x1, $y1);
        $pdf->SetFont('Arial', 'B', 12);
        $length = $pdf->GetStringWidth("Riepilogo Ordine ". $order->id);
        $pdf->Cell($length, 2, ' Riepilogo Ordine N '. $order->id);
    }

    public function intestazioneDestraPDF($pdf, $order) {

        $billing = $order->billing_address();

        $intestatario = $billing->name . ' ' . $billing->surname;
        $riga = "";
        $riga.= strtoupper($intestatario) . "\n";
        $riga.= $billing->address . "\n";
        $cap = $billing->zipcode . " ";
        $citta = $billing->city . " ";
        $riga.= $cap . $citta . " \n";
        $riga.= "Tel. ".$billing->phone . " \n";

        $r1 = $pdf->w - 80;
        $r2 = $r1 + 68;
        $y1 = 26;
        $y2 = 0;
        $pdf->RoundedRect($r1 - 10, 52, ($r2 - $r1) - 12, ($y2 - $y1) - 18, 0, 'D');
        $pdf->SetXY($r1 - 8, $y1 - 15);
        $pdf->SetFont("Arial", "B", 8);
        $pdf->MultiCell(60, 1, 'Spett.le');
        $pdf->SetXY($r1 - 5, $y1 - 13);
        $pdf->SetFont("Arial", "", 7);
        $pdf->MultiCell(60, 4, $riga);

        $shipping = $order->shipping_address();
        if ( ! $shipping )
            $shipping = $billing;

        $intestatario = $shipping->name . ' ' . $shipping->surname;
        $riga = "";
        $riga.= strtoupper($intestatario) . "\n";
        $riga.= $shipping->address . "\n";
        $cap = $shipping->zipcode . " ";
        $citta = $shipping->city . " ";
        $riga.= $cap . $citta . " \n";
        $riga.= "Tel. ".$shipping->phone . " \n";


        $pdf->SetXY($r1 - 8, $y1 + 6);
        $pdf->SetFont("Arial", "B", 8);
        $pdf->MultiCell(60, 1, 'Destinatario Merce');
        $pdf->SetXY($r1 - 5, $y1 + 8);
        $pdf->SetFont("Arial", "", 7);
        $pdf->MultiCell(60, 4, $riga);
    }

    public function addTabellaInformazioni($pdf, $ordine) {

        $shipping = $ordine->shipping_address();
        if ( ! $shipping )
            $shipping = $ordine->billing_address();
        //PRIMA RIGA
        $pdf->SetFont("Arial", "B", 9);
        $r1 = 10;
        $r2 = $r1 + 30;
        $y1 = 61;
        $y2 = $y1 + 10;
        $pdf->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 0, 'D');
        $pdf->SetXY($r1 + 10, $y1 + 1);
        $pdf->Cell(3, 6, "Cod. Cli", '', '', "C");
        if ($ordine->id_customer) {
            $pdf->SetXY($r1 + 10, $y1 + 5);
            $pdf->Cell(3, 6, "$ordine->id_customer", '', '', "C");
        }

        $r1 = 40;
        $r2 = $r1 + 15;
        $y1 = 61;
        $y2 = $y1 + 10;
        $pdf->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 0, 'D');
        $pdf->SetXY($r1 + 5, $y1 + 1);
        $pdf->Cell(3, 4, 'Valuta', '', '', "C");
        $pdf->SetXY($r1 + 5, $y1 + 5);
        $pdf->Cell(3, 6, "Euro", '', '', "C");

        $r1 = 55;
        $r2 = $r1 + 40;
        $y1 = 61;
        $y2 = $y1 + 10;
        $pdf->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 0, 'D');
        $pdf->SetXY($r1 + 11, $y1 + 1);
        $pdf->Cell(3, 4, 'Data', '', '', "C");
        $pdf->SetXY($r1 + 14, $y1 + 5);
        $date = date_create($ordine->date);
        $pdf->Cell(3, 6, date_format($date, 'd/m/Y'), '', '', "C");

        $r1 = 95;
        $r2 = $r1 + 60;
        $y1 = 61;
        $y2 = $y1 + 10;
        $pdf->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 0, 'D');
        $pdf->SetXY($r1 + 10, $y1 + 1);
        $pdf->Cell(8, 4, 'Tipo pagamento', '', '', "C");
        $pdf->SetXY($r1 + 14, $y1 + 5);
        $pdf->Cell(3, 6, $ordine->typePayment->title, '', '', "C");


        $r1 = 155;
        $r2 = $r1 + 45;
        $y1 = 61;
        $y2 = $y1 + 10;
        $pdf->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 0, 'D');
        $pdf->SetXY($r1 + 8, $y1 + 1);
        $pdf->Cell(11, 4, 'Tipo Spedizione', '', '', "C");
        $pdf->SetXY($r1 + 7, $y1 + 5);
        $pdf->Cell(16, 6, $ordine->typeShip->title, '', '', "C");
    }



}