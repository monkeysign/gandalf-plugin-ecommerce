<?php
//Controller Namespace
const _P_ECOMMERCE = "\\Plugins\\ECOMMERCE\\Controllers\\";
// Admin group
router()->group( [ 'middleware' => [ 'web' ] ], function () {
    //auth route
    router()->group( [ 'middleware' => [ 'auth' ], 'prefix' => 'admin/ecommerce' ], function () {

        router()->group( [ 'prefix' => 'mws' ], function () {
            router()->get( '/test', [ 'as' => 'admin.ecommerce.mws.test', 'uses' => _P_ECOMMERCE . 'MWS@test' ] );
        } );

        // Brand
        router()->group( [ 'prefix' => 'brand' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.brand.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.brand.list', 'uses' => _P_ECOMMERCE . 'Brands@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.brand.add', 'uses' => _P_ECOMMERCE . 'Brands@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.brand.update', 'uses' => _P_ECOMMERCE . 'Brands@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.brand.save', 'uses' => _P_ECOMMERCE . 'Brands@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.brand.delete', 'uses' => _P_ECOMMERCE . 'Brands@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.brand.deletegroup', 'uses' => _P_ECOMMERCE . 'Brands@deleteGroup' ]);
        } );

        //Feature
        router()->group( [ 'prefix' => 'feature' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.feature.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.feature.list', 'uses' => _P_ECOMMERCE . 'Features@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.feature.add', 'uses' => _P_ECOMMERCE . 'Features@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.feature.update', 'uses' => _P_ECOMMERCE . 'Features@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.feature.save', 'uses' => _P_ECOMMERCE . 'Features@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.feature.delete', 'uses' => _P_ECOMMERCE . 'Features@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.feature.deletegroup', 'uses' => _P_ECOMMERCE . 'Features@deleteGroup' ]);
            router()->post('/ajax/feature', ['as' => 'admin.ecommerce.feature.ajax', 'uses' => _P_ECOMMERCE . 'Features@ajaxFeature']);
        } );

        //Product Type
        router()->group( [ 'prefix' => 'producttype' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.producttype.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.producttype.list', 'uses' => _P_ECOMMERCE . 'ProductTypes@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.producttype.add', 'uses' => _P_ECOMMERCE . 'ProductTypes@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.producttype.update', 'uses' => _P_ECOMMERCE . 'ProductTypes@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.producttype.save', 'uses' => _P_ECOMMERCE . 'ProductTypes@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.producttype.delete', 'uses' => _P_ECOMMERCE . 'ProductTypes@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.producttype.deletegroup', 'uses' => _P_ECOMMERCE . 'ProductTypes@deleteGroup' ]);
            router()->post('/ajax/feature', ['as' => 'admin.ecommerce.producttype.ajax', 'uses' => _P_ECOMMERCE . 'ProductTypes@ajaxFeature']);
        } );

        //Attribute
        router()->group( [ 'prefix' => 'attribute' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.attribute.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.attribute.list', 'uses' => _P_ECOMMERCE . 'Attributes@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.attribute.add', 'uses' => _P_ECOMMERCE . 'Attributes@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.attribute.update', 'uses' => _P_ECOMMERCE . 'Attributes@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.attribute.save', 'uses' => _P_ECOMMERCE . 'Attributes@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.attribute.delete', 'uses' => _P_ECOMMERCE . 'Attributes@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.attribute.deletegroup', 'uses' => _P_ECOMMERCE . 'Attributes@deleteGroup' ]);
        } );

        //Category
        router()->group( [ 'prefix' => 'category' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.category.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.category.list', 'uses' => _P_ECOMMERCE . 'Categories@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.category.add', 'uses' => _P_ECOMMERCE . 'Categories@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.category.update', 'uses' => _P_ECOMMERCE . 'Categories@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.category.save', 'uses' => _P_ECOMMERCE . 'Categories@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.category.delete', 'uses' => _P_ECOMMERCE . 'Categories@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.category.deletegroup', 'uses' => _P_ECOMMERCE . 'Categories@deleteGroup' ]);
        } );


        //Product
        router()->group( [ 'prefix' => 'product' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.product.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.product.list', 'uses' => _P_ECOMMERCE . 'Products@listAll' ] );
            router()->get( '/list/ajax', [ 'as' => 'admin.ecommerce.product.listAjax', 'uses' => _P_ECOMMERCE . 'Products@listAllAjax' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.product.add', 'uses' => _P_ECOMMERCE . 'Products@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.product.update', 'uses' => _P_ECOMMERCE . 'Products@form' ]);
            router()->post('/massive', [ 'as' => 'admin.ecommerce.product.updateMassive', 'uses' => _P_ECOMMERCE . 'Products@form' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.product.save', 'uses' => _P_ECOMMERCE . 'Products@save' ]);
            router()->post('/saveGroup', [ 'as' => 'admin.ecommerce.product.saveGroup', 'uses' => _P_ECOMMERCE . 'Products@saveGroup' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.product.delete', 'uses' => _P_ECOMMERCE . 'Products@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.product.deletegroup', 'uses' => _P_ECOMMERCE . 'Products@deleteGroup' ]);
            router()->post('/galleryUpload', [ 'as' => 'admin.ecommerce.product.galleryupload', 'uses' => _P_ECOMMERCE . 'Products@galleryUpload' ]);
            router()->post('/galleryDelete', [ 'as' => 'admin.ecommerce.product.gallerydelete', 'uses' => _P_ECOMMERCE . 'Products@galleryDelete' ]);
            router()->post('/ean', [ 'as' => 'admin.ecommerce.product.ean', 'uses' => _P_ECOMMERCE . 'Products@eanSearch' ]);
            router()->post('/scarico', [ 'as' => 'admin.ecommerce.product.scarico', 'uses' => _P_ECOMMERCE . 'Products@scarico' ]);
        } );



        //Order
        router()->group( [ 'prefix' => 'order' ], function () {
            router()->get('', [ 'as' => 'admin.ecommerce.order.base' ]);
            router()->get( '/list', [ 'as' => 'admin.ecommerce.order.list', 'uses' => _P_ECOMMERCE . 'Orders@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.ecommerce.order.add', 'uses' => _P_ECOMMERCE . 'Orders@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.ecommerce.order.update', 'uses' => _P_ECOMMERCE . 'Orders@form' ]);
            router()->get('/pdf/{id}', [ 'as' => 'admin.ecommerce.order.pdf', 'uses' => _P_ECOMMERCE . 'Orders@pdfOrdine' ]);
            router()->post('/save', [ 'as' => 'admin.ecommerce.order.save', 'uses' => _P_ECOMMERCE . 'Orders@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.ecommerce.order.delete', 'uses' => _P_ECOMMERCE . 'Orders@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.ecommerce.order.deletegroup', 'uses' => _P_ECOMMERCE . 'Orders@deleteGroup' ]);
        } );
    });

});