<?php
/**
 * Created by PhpStorm.
 * User: Riccardo
 * Date: 30/01/2018
 * Time: 11:19
 */

namespace Plugins\ECOMMERCE;


class Plugin extends \Ring\Plugin\Pluggable {
	static function register(){
		parent::register();
		require __DIR__ .'/hooks.php';
	}
}