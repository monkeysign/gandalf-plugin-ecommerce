@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.brand.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>Visualizzazione Brand</strong>
                            @else
                                Nuovo Brand
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.ecommerce.brand.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Brand *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->title}}" required
                                                       data-toggle="validator" type="text" name="item[title]"
                                                       id="name" class="form-control"
                                                       placeholder="Nome">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Codice</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->code}}" data-toggle="validator" type="text"
                                                       name="item[code]"
                                                       id="name" class="form-control"
                                                       placeholder="Codice">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label
                                                    title="Stato Pratica"
                                                    class="control-label">Stato</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[status]" class="form-control select-choose">
                                                    <option
                                                            @if ( $record->status == 0)
                                                            selected @endif
                                                            value="0">Bozza
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 1)
                                                            selected @endif
                                                            value="1">Pubblicato
                                                    </option>

                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Ordine</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->order}}" data-toggle="validator" type="text"
                                                       name="item[order]"
                                                       id="name" class="form-control"
                                                       placeholder="Ordine">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-3">
                                        <label for="input-file-now">Immagine in evidenza</label>
                                        <input type="file" id="imghighlight" name="imghighlight"
                                               {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                               class="dropify"
                                               data-default-file="@if($record->meta('imghighlight')){{config('httpmedia')."ecommerce/brand/".$record->meta('imghighlight')}}@else{{''}}@endif"/>
                                        <input id="imghighlight-old" type="hidden" name="imghighlight-old"
                                               value="{{$record->meta('imghighlight')}}">
                                        <script>
                                            $(function () {
                                                // upload
                                                var __imghighlight = $("#imghighlight").dropify();
                                                __imghighlight.on('dropify.afterClear', function (event, element) {
                                                    $('#imghighlight-old').val('');
                                                });
                                            })
                                        </script>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- Dropify -->
    <script src="{{asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>



    <script>
        $(function () {


        });
    </script>
@endsection