@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

        @include('ecommerce.producttype.header')

        <!-- START WIDGETS -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="table-responsive">
                            <table id="tabella" class="table m-t-30 table-hover contact-list">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="selectallrow"></th>
                                    <th>Tipologia</th>
                                    <th>Stato</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Tipologia</th>
                                    <th>Stato</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach ($table as $row)
                                    <tr>
                                        <td><input type="checkbox" class="selectrow" value="{{ $row->id }}"></td>
                                        <td>{{ $row->title }}</td>
                                        <td>
                                            @if ($row->status == 1)
                                                <span class="label label-success">Pubblicato</span>
                                            @elseif ($row->status == 2)
                                                <span class="label label-default">Bozza</span>
                                            @else
                                                <span class="label label-success">Pubblicato</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="...">
                                                <a href=" {{ path_for('admin.ecommerce.producttype.update', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                <a href="#" data-path="{{ path_for('admin.ecommerce.producttype.delete', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-danger delete-item"><span
                                                            class='fa fa-trash-o'></span></a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="deleteAll" data-path="{{ path_for('admin.ecommerce.producttype.deletegroup') }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script>
        $(document).ready(function () {

            var table = $('#tabella').DataTable({
                responsive: true,
                displayLength: 25,
                "order": 0,
                {{--
                language: {
                "url": "{{assetsBack}}lang/datatable/datatable-{% trans %}language{% endtrans %}.json"
                },
                --}}
                columns: [
                    {"searchable": false, "sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"searchable": false, "sortable": false}
                ]
            });

//la trovi in function_custom.js
            tabellaInit(table);
        });
    </script>
@endsection