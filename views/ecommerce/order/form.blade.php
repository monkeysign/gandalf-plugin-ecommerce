@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.order.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-9">
                                <h2>
                                    @if($record->id)
                                        <strong>Visualizzazione Ordine</strong>
                                    @else
                                        Nuovo Ordine
                                    @endif
                                </h2>
                            </div>
                            <div class="col-md-3">



                                <a href=" {{ path_for('admin.ecommerce.order.pdf', ['id' => $record->id]) }}"
                                   id="print-button" class="btn btn-success" target="u_blank"> <i class="fa fa-print"></i></a>

                            </div>

                        </div>


                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.ecommerce.order.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <input  type="hidden" value='@if($record->id){{ $record->status }}@else{{0}}@endif' name='status_old'>
                            <div class="form-body" id="form-print">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Id</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->id}}" data-toggle="validator" disabled type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Titolo</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->title}}" disabled="" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Data Ordine</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{date('d/m/Y', strtotime($record->date))}}" disabled="" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Totale</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->total_label}}" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    @if($record->orderProduct)
                                    <div class="m-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <strong>Prodotto</strong>
                                            </div>
                                            <div class="col-md-4">
                                                <strong>Permalink</strong>
                                            </div>
                                            <div class="col-md-2">
                                                <strong>Stato Prod.</strong>
                                            </div>
                                            <div class="col-md-2">
                                                <strong>Quantit&agrave;</strong>
                                            </div>
                                        </div>
                                        @foreach($record->orderProduct as $prodotto)
                                            <div class="row">
                                                <div class="col-md-4">
                                                    {{$prodotto->title}}
                                                </div>
                                                <div class="col-md-4">
                                                    <a target="u_blank" href="{{path_for('product', ['permalink' => $prodotto->product->getPermalink()])}}">
                                                        {{$prodotto->product->permalink}}
                                                    </a>
                                                </div>
                                                <div class="col-md-2">
                                                    @if($prodotto->product->state_product==1)
                                                             Usato
                                                         @else
                                                             Nuovo
                                                         @endif
                                                </div>
                                                <div class="col-md-2">
                                                    {{$prodotto->quantity}}
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    @endif


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Cliente</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->billing_address()->name}} {{$record->billing_address()->surname}}" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Email Cliente</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->billing_address()->email}}" disabled  data-toggle="validator" type="text" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note Cliente</label>
                                            <div class="input-group">
                                                <textarea disabled style="width: 100%">{{$record->note}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Stato Ordine"
                                                    class="control-label">Stato Ordine</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[status]" class="form-control select-choose" id="select_status">
                                                    <option
                                                            @if ( $record->status == 0)
                                                            selected @endif
                                                            value="0">In attesa di pagamento
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 1)
                                                            selected @endif
                                                            value="1">In Lavorazione
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 2)
                                                            selected @endif
                                                            value="2">Spedito
                                                    </option>
                                                    <!--<option
                                                            @if ( $record->status == 3)
                                                            selected @endif
                                                            value="3">Concluso
                                                    </option>-->
                                                    <option
                                                            @if ( $record->status == 4)
                                                            selected @endif
                                                            value="4">Annullato
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 5)
                                                            selected @endif
                                                            value="5">Rimborsato
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 6)
                                                            selected @endif
                                                            value="6">Ritirato in sede
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Metodo Pagamento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->typePayment->title}}" disabled  data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Stato Pagamento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[payment_status]" class="form-control select-choose">
                                                    <option
                                                            @if ( $record->payment_status == 0)
                                                            selected @endif
                                                            value="0">Non Pagato
                                                    </option>
                                                    <option
                                                            @if ( $record->payment_status == 1)
                                                            selected @endif
                                                            value="1">Pagato
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Data Pagamento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->payment_date}}" name="item[payment_date]" data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>-->

                                </div>
                                <div class="row @if($record->status == 5){{"visible"}}@else {{"hide"}}@endif" id="box_rimborso">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Data Rimborso</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->refund_date}}" name="item[refund_date]" data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Rimborso</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->refund}}" name="item[refund]" data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                @if($record->payment_type==2)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Codice Transazione PayPal</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->code_payment}}" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Utente PayPal</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->user_paypal}}" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Email PayPal</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->email_paypal}}" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Spedizione</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->typeShip->title}}" name="item[sped]" disabled data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tracking Code</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->tracking}}" name="item[tracking]" data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Contenuto Scaricabile</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->downloadable}}" name="item[downloadable]" data-toggle="validator" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @include('ecommerce.order.address_order')
                            </div>
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        jQuery(document).ready(function () {
            $('#select_status').change(function () {
                var valOption = $(this).val();
                if (valOption == 5) {
                    $('#box_rimborso').removeClass("hide");
                } else {
                    $('#box_rimborso').addClass("hide");
                }
            });
        });
    </script>
@endsection