<?php
$shipping = $record->shipping_address();
if ( ! $shipping )
	$shipping = new  Plugins\ECOMMERCE\Models\OrderAddress();

$billing = $record->billing_address();
if ( ! $billing )
	$billing = new  Plugins\ECOMMERCE\Models\OrderAddress();
?>

<div class="row">
    <div class="col-6">
        <h4><strong>Indirizzo di Spedizione</strong></h4>
        <hr>
        <div class="row">
            <input type="hidden" name="billing_address[id]" value="{{$billing->id}}">
            <input type="hidden" name="billing_address[type]" value="2">
            <input type="hidden" name="billing_address[selected]" value="1">
            <div class="col-12 p-3 pt-5 bg-faded">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Nome</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->name}}"
                                       data-toggle="validator" disabled="" type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Cognome</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->surname}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->email}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Indirizzo</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->address}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Nazione</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->nation}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[nation]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Provincia</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->state}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[state]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Città</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->city}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[city]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">CAP</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$billing->zipcode}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[zipcode]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Telefono</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-mobile"></i>
                                </div>
                                <input value="{{$billing->phone}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[phone]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-6">
        <h4><strong>Indirizzo Secondario</strong></h4>
        <hr>
        <div class="row">
            <input type="hidden" name="shipping_address[id]" value="{{$shipping->id}}">
            <input type="hidden" name="shipping_address[type]" value="1">
            <input type="hidden" name="shipping_address[selected]" value="1">
            <div class="col-12 p-3 pt-5 bg-faded">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Nome</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->name}}"
                                       data-toggle="validator" disabled="" type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Cognome</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->surname}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->email}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Indirizzo</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->address}}"
                                       data-toggle="validator" disabled=""  type="text" name="billing_address[address]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Nazione</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->nation}}"
                                       data-toggle="validator" disabled type="text" name="shipping_address[nation]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Provincia</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->state}}"
                                       data-toggle="validator" disabled type="text" name="shipping_address[state]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Città</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->city}}"
                                       data-toggle="validator" disabled type="text" name="shipping_address[city]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">CAP</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-world"></i>
                                </div>
                                <input value="{{$shipping->zipcode}}"
                                       data-toggle="validator" disabled type="text" name="shipping_address[zipcode]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label title="Campo obbligatorio"
                                   class="control-label">Telefono</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="ti-mobile"></i>
                                </div>
                                <input value="{{$shipping->phone}}"
                                       data-toggle="validator" disabled type="text" name="shipping_address[phone]"
                                       class="form-control">
                            </div>
                            <span class="help-block with-errors"> </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



</div>