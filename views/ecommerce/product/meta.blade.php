{{-- GALLERY --}}
<div class="row mt-4 mb-4">
    <h3 class="col-12 mb-5">Gallery
        <button data-toggle="modal" data-target=".bs-example-modal-sm" type="button"
                class="ml-3 btn btn-primary" id="add-galley-item">Aggiungi alla gallery
        </button>
    </h3>
    <div id="gallery-cont" class="row">
        <div class="flex-gallery ">
            <input name="meta[gallery]" type="hidden" value="">
            @if($record->meta('gallery'))
                @foreach(unserialize($record->meta('gallery')) as $media)
                    <div class="flex-item">
                        <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($media) }}" class="mw-100">
                        <input name="meta[gallery][]" type="hidden" value="{{basename($media)}}">
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
{{-- Piazzo un modal per l'upload in ajax dei file --}}
@section('modal')
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Upload File</h4>
                </div>
                <div class="modal-body">
                    <form id="gallery_form"
                          action="{{ path_for('admin.ecommerce.product.galleryupload') }}"
                          method="POST"
                          enctype="multipart/form-data">

                        <input type="file" id="gallery_input" name="gallery"
                               {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                               class="dropify"
                        />
                    </form>
                    {{--<div class="flex-gallery flex-selection">

                        @foreach(media_gallery() as $media)
                            <div class="flex-item">
                                <img src="{{ config('httpmedia'). basename($media) }}" class="mw-100">
                            </div>
                        @endforeach
                    </div>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Chiudi
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script>
        $(function () {

            $('#gallery_input').dropify().on('change', function () {
                $('#gallery_form').submit();
            });


            $('#gallery_form').on('submit', function (e) {
                var $form = $('#gallery_form')
                var formData = new FormData($form[0]);

                $.ajax({
                    method: "POST",
                    url: $form.attr("action"),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.state === true) {
                            $('#gallery-cont .flex-gallery').append(
                                '<div class="flex-item">\n' +
                                '<img src="' + data.file + '" class="mw-100">\n' +
                                '<input name="meta[gallery][]" type="hidden" value="' + data.filename + '">\n' +
                                '</div>'
                            )
                        }
                        $('#gallery_form .dropify-clear').click();
                        $('#form-save').submit();
                    }
                });
                return false;
            })

            $(document).on('click', '.flex-gallery .flex-item', function () {
                var el = $(this)
                $.ajax({
                    method: "POST",
                    url: '{{ path_for('admin.ecommerce.product.gallerydelete') }}',
                    data: {'filename': el.find('input').val()},
                    success: function () {
                        el.remove();
                        $('#form-save').submit();
                    }
                })
            })

        });
    </script>
@endsection