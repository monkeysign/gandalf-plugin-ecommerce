@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.product.header')

            <div class="row mb-5">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="updateAll" data-path="#"
                       class="btn btn-sm btn-info update-item"><span class='fa fa-pencil'></span> Moifica Massiva</a>
                    <a id="deleteAll" data-path="{{ path_for('admin.ecommerce.product.deletegroup') }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>

            <!-- START WIDGETS -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <table id="tabella" class="table-responsive" style="width:100%">
                            <thead>
                            <tr>
                                <th class="no-filter checkalltable"></th>
                                <th>EAN</th>
                                <th>Prodotto</th>
                                <th>Categoria</th>
                                <th>Permalink</th>
                                <th>Quantità</th>
                                <th>Prezzo</th>
                                <th>Tipologia</th>
                                <th>Peso</th>
                                <th>Stato Prod.</th>
                                <th class="no-filter">Azioni</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="no-filter"></th>
                                <th>EAN</th>
                                <th>Prodotto</th>
                                <th>Categoria</th>
                                <th>Permalink</th>
                                <th>Prezzo</th>
                                <th>Quantità</th>
                                <th>Tipologia</th>
                                <th>Peso</th>
                                <th>Stato Prod.</th>
                                <th class="no-filter">Azioni</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script>

      var oTable;
      $(document).ready(function () {
        oTable = $('#tabella').DataTable({

          "aoColumns": [
            {"searchable": false, "sortable": false, "bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false}
          ],
          "pageLength": 50,
          "lengthMenu": [10, 25, 50, 75, 100, 200, 500],
          "ordering": false,
          "bServerSide": true, //se voglio effettuare filtering o pagining da remoto
          "sAjaxSource": "{{ path_for('admin.ecommerce.product.listAjax') }}"

        });


        $('#tabella tfoot th').each(function () {
          if (!$(this).hasClass('no-filter')) {
            var title = $('#tabella thead th').eq($(this).index()).text();
            $(this).html('<input class="form-control" style="width:100%;" type="text" placeholder="' + title + '" />');
          } else {
            $(this).html('');
          }
        });

        $('#tabella tfoot input[type="text"]').on('keyup change', function () {
          var indice = $(this).parent().index('#tabella tfoot th');
          oTable.column(indice).search(this.value).draw();
        });


        $('#tabella thead th').each(function () {
          if (!$(this).hasClass('no-filter')) {
            var title = $('#tabella thead th').eq($(this).index()).text();
            $(this).html('<input class="form-control" style="width:100%;" type="text" placeholder="' + title + '" />');
          } else {
            $(this).html('');
            if ($(this).hasClass('checkalltable')) $(this).html('<input type="checkbox" id="checkAll">')
          }
        });

        $('#tabella thead input[type="text"]').on('keyup change', function () {
          var indice = $(this).parent().index('#tabella thead th');
          console.log(indice, this.value)
          oTable.column(indice).search(this.value).draw();
        });

        $("#checkAll").click(function () {
          $('#tabella tbody td input:checkbox').not(this).prop('checked', this.checked);
        });

        $('#updateAll').on('click', function () {
          var selected = '';
          $('#tabella tbody td input:checkbox:checked').each(function () {
            selected += ($(this).val()) + ',';
          });
          $form = '<form id="massiveForm" method="POST" action="{{ path_for('admin.ecommerce.product.updateMassive') }}"><input type="hidden" name="allIds" value="' + selected + '" /></form>';
          $('body').append($form);
          $('#massiveForm').submit();
        })

        $('#tabella tbody').on('click', '.delete-item', function () {
          var buttonclick = $(this);
          if (confirm('Sei sicuro di voler cancellare questo record?')) {
            $.ajax({
              url: '' + $(this).data('path'),
              dataType: "json",
              success: function (response) {
                if (response.result) {
                  buttonclick.parents('tr').fadeOut();
                } else {
                  alert('ERRORE NELLA CANCELLAZIONE');
                }
              }
            });
          }
        });


      });
    </script>
@endsection