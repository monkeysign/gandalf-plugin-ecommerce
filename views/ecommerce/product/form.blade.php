<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.product.header')

            <div class="row">
                <div class="col-md-8">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>Visualizzazione Prodotto</strong>
                            @elseif($allIds)
                                <strong>Modifica Massiva</strong>
                            @else
                                Nuovo Prodotto
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.ecommerce.product.save') }}" method="POST"
                              enctype="multipart/form-data">
                            @if(!$allIds)<input id="item_id_hidden" type="hidden"
                                                value='@if($record->id){{ $record->id }}@else{{0}}@endif'
                                                name='item[id]'>@endif

                            <input id="item_id_hidden_ids" type="hidden"
                                   value='@if($allIds){{ $allIds }}@else{{0}}@endif' name='allIds'>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva Prodotto
                                        </button>
                                    </div>
                                </div>
                                <div class="row mt-4 mb-4">
                                    @if(!$allIds)
                                        <div class="col-12">
                                            <label for="input-file-now">Immagine in evidenza</label>
                                            <input type="file" id="imghighlight" name="imghighlight"
                                                   {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                   class="dropify"
                                                   data-default-file="@if($record->meta('imghighlight')){{config('httpmedia').'ecommerce/prodotti/'.$record->meta('imghighlight')}}@else{{''}}@endif"/>
                                            <input id="imghighlight-old" type="hidden" name="imghighlight-old"
                                                   value="{{$record->meta('imghighlight')}}">
                                            <script>
                                              $(function () {
                                                // upload
                                                var __imghighlight = $("#imghighlight").dropify();
                                                __imghighlight.on('dropify.afterClear', function (event, element) {
                                                  $('#imghighlight-old').val('');
                                                });
                                              })
                                            </script>
                                        </div>
                                    @endif
                                </div>
                                @if(!$allIds)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label title="Campo obbligatorio"
                                                       class="control-label">Prodotto *</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="ti-user"></i>
                                                    </div>
                                                    <input value="{{$record->title}}" required
                                                           data-toggle="validator" type="text" name="item[title]"
                                                           id="name" class="form-control"
                                                           placeholder="Nome">
                                                </div>
                                                <span class="help-block with-errors"> </span>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <label for="permalink" class="control-label">Permalink:</label>

                                            <div>
                                                <input data-toggle="validator" type="text" id="permalink"
                                                       placeholder="Permalink"
                                                       name="item[permalink]"
                                                       class="form-control"
                                                       value="{{$record->permalink}}">
                                            </div>

                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Rilevanza</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <input value="{{$record->rilevance}}"
                                                data-toggle="validator" type="numeric" name="item[rilevance]"
                                                       id="rilevance" class="form-control"
                                                       placeholder="Rilevanza">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label
                                                    title="Stato Pratica"
                                                    class="control-label">Stato</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[status]" class="form-control select-choose">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif

                                                    <option
                                                            @if ( $record->status == 1)
                                                            selected @endif
                                                            value="1">Pubblicato
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 2)
                                                            selected @endif
                                                            value="2">Bozza
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Prezzo *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->price}}" @if(!$allIds) required @endif
                                                data-toggle="validator" type="numeric" name="item[price]"
                                                       id="prie" class="form-control"
                                                       placeholder="Prezzo">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                <!--
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Categoria</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select @if(!$allIds) required @endif name="item[id_category]"
                                                        data-placeholder="Seleziona la categoria"
                                                        class="select2 select-choose form-control" id="select-category">
                                                    <option value="">Seleziona una categoria</option>
                                                    @foreach($allCategories as $category)
                                    <option @if($category->id==$record->id_category){{"selected"}}@endif value="{{$category->id}}">{{$category->path_label}}</option>
                                                    @endforeach
                                        </select>
                                    </div>
                                    <span class="help-block with-errors"> </span>
                                </div>
                            </div>
-->

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Brand</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[id_brand]"
                                                        data-placeholder="Seleziona brand"
                                                        class="select2 select-choose form-control"
                                                >
                                                    <option value="">Seleziona un brand</option>
                                                    @foreach($allBrands as $brand)
                                                        <option @if($brand->id==$record->id_brand){{"selected"}}@endif value="{{$brand->id}}">{{$brand->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Novit&agrave;</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[novelty]"
                                                        data-placeholder="Novità"
                                                        class="select2 select-choose form-control">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option value="0">No</option>
                                                    <option @if($record->novelty==1){{"selected"}}@endif value="1">Si
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Offerta</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[offer]"
                                                        data-placeholder="Offerta"
                                                        class="select2 select-choose form-control">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option value="0">No</option>
                                                    <option @if($record->offer==1){{"selected"}}@endif value="1">Si
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Prezzo in Offerta</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->price_offer}}"
                                                       data-toggle="validator" type="numeric" name="item[price_offer]"
                                                       class="form-control" placeholder="Prezzo in offerta">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Best Seller</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[best_seller]"
                                                        data-placeholder="Best Seller"
                                                        class="select2 select-choose form-control">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option value="0">No</option>
                                                    <option @if($record->best_seller==1){{"selected"}}@endif value="1">Si
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Scelti per voi</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[picked]"
                                                        data-placeholder="Best Seller"
                                                        class="select2 select-choose form-control">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option value="0">No</option>
                                                    <option @if($record->picked==1){{"selected"}}@endif value="1">Si
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="ean"
                                                   class="control-label">EAN</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->ean}}"
                                                       data-toggle="validator" type="text" name="item[ean]"
                                                       class="form-control" placeholder="EAN">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="ean"
                                                   class="control-label">EAN Secondario</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->ean2}}"
                                                       data-toggle="validator" type="text" name="item[ean2]"
                                                       class="form-control" placeholder="EAN Secondario">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="sku"
                                                   class="control-label">SKU</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->sku}}"
                                                       data-toggle="validator" type="text" name="item[sku]"
                                                       class="form-control" placeholder="SKU">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Quantit&agrave;</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->quantity}}" @if(!$allIds) required @endif
                                                data-toggle="validator" type="numeric" name="item[quantity]"
                                                       class="form-control" placeholder="Quantità">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                <!--  <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Tipologia"
                                                   class="control-label">Tipologia Prodotto</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[type]"
                                                        data-placeholder="Tipologia Prodotto"
                                                        class="select2 select-choose form-control">
                                                    <option value="0">Normale</option>
                                                    <option @if($record->type==1){{"selected"}}@endif value="1">Virtuale</option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Usato Garantito</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="meta[usato_garantito]"
                                                        data-placeholder="Usato Garantito"
                                                        class="select2 select-choose form-control">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option @if($record->meta('usato_garantito')==0){{"selected"}}@endif value="0">
                                                        No
                                                    </option>
                                                    <option @if($record->meta('usato_garantito')==1){{"selected"}}@endif value="1">
                                                        Si
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Grado Usato"
                                                   class="control-label">Grado Usato</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[grade]"
                                                        data-placeholder="Grado Usato"
                                                        class="select2 select-choose form-control" id="grade">
                                                    <option value="">- Seleziona -</option>
                                                    <option @if($record->grade == 'A'){{"selected"}}@endif value="A">
                                                        Grado A
                                                    </option>
                                                    <option @if($record->grade == 'B'){{"selected"}}@endif value="B">
                                                        Grado B
                                                    </option>
                                                    <option @if($record->grade == 'AB'){{"selected"}}@endif value="AB">
                                                        Grado AB
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Grado Usato"
                                                   class="control-label">Mac Potenziato</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[mac_powerup]"
                                                        data-placeholder="MAC Potenziato"
                                                        class="select2 select-choose form-control" id="mac_powerup">
                                                    <option value="">- Seleziona -</option>
                                                    <option @if($record->mac_powerup == 1){{"selected"}}@endif value="1">
                                                        Si
                                                    </option>
                                                    <option @if($record->mac_powerup == 0){{"selected"}}@endif value="0">
                                                        No
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Esporta AMAZON"
                                                   class="control-label">Esporta AMAZON</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[amazon]"
                                                        data-placeholder="Esporta Amazon"
                                                        class="select2 select-choose form-control" id="amazon">
                                                    <option value="">- Seleziona -</option>
                                                    <option @if($record->amazon == 1){{"selected"}}@endif value="1">
                                                        Si
                                                    </option>
                                                    <option @if($record->amazon == 0){{"selected"}}@endif value="0">
                                                        No
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Esporta FACEBOOK"
                                                   class="control-label">Esporta FACEBOOK</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[facebook]"
                                                        data-placeholder="Esporta Facebook"
                                                        class="select2 select-choose form-control" id="facebook">
                                                    <option value="">- Seleziona -</option>
                                                    <option @if($record->facebook == 1){{"selected"}}@endif value="1">
                                                        Si
                                                    </option>
                                                    <option @if($record->facebook == 0){{"selected"}}@endif value="0">
                                                        No
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Stato Prodotto</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[state_product]"
                                                        data-placeholder="Stato del prodotto"
                                                        class="select2 select-choose form-control" id="state_product">
                                                    @if($allIds)
                                                        <option value=""> -</option>@endif
                                                    <option value="0">Nuovo</option>
                                                    <option @if($record->state_product==1){{"selected"}}@endif value="1">
                                                        Usato
                                                    </option>
                                                    <option @if($record->state_product==2){{"selected"}}@endif value="2">
                                                        In Arrivo
                                                    </option>
                                                    <option @if($record->state_product==3){{"selected"}}@endif value="3">
                                                        In Rientro
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 @if($record->state_product==2 || $record->state_product==3){{"visible"}}@else{{"hide"}}@endif"
                                         id="box_data_arrivo">
                                        <div class="form-group">
                                            <label title="Campo Novità"
                                                   class="control-label">Data Arrivo (accetta HTML)</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->meta('arrival_date')}}" type="text"
                                                       name="meta[arrival_date]" class="form-control"/>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>

                              <?php $riga = 0; ?>
                                @if(count($compatibilita))

                                    @foreach ($compatibilita as $compatib)
                                        @if($riga % 2 ==0 )
                                            <div class="row">
                                                @endif
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label title="Campo"
                                                               class="control-label">{{$compatib->title}}</label>
                                                        <div class="input-group">
                                                            <select multiple name="compatibilita[]"
                                                                    data-placeholder="Seleziona la compatibilità"
                                                                    class="form-control select2 select2-multiple">
                                                                @if($compatib->attributes)
                                                                    @foreach ($compatib->attributes as $attribute)
                                                                        <option @if(in_array($attribute->id,$attributeS)){{"selected"}}@endif value='{{$attribute->id}}'>{{$attribute->title}} </option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <span class="help-block with-errors"> </span>
                                                    </div>
                                                </div>
                                              <?php $riga++; ?>
                                                @if($riga % 2 ==0 )
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            <!-- Chiudo eventuali div aperti -->
                                @if($riga % 2 !=0 )
                            </div>
                            @endif

                            <div class="row @if($record->id || $allIds){{"visible"}}@else {{"hide"}}@endif"
                                 id="content-feature">
                                <strong>Associa Variante</strong>
                                <div class="col-12" id="box-feature">
                                    @if(count($features))
                                        @foreach ($features as $feature)
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label title="Campo"
                                                           class="control-label">{{$feature->title}}</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <select name="feature[feature_{{$feature->id}}]"
                                                                data-placeholder="Seleziona la categoria"
                                                                class="select2 select-choose form-control"
                                                        >
                                                            <option value="">Seleziona un valore</option>

                                                            @if($feature->attributes)
                                                                @foreach ($feature->attributes as $attribute)
                                                                    <option @if(in_array($attribute->id,$attributeS)){{"selected"}}@endif value='{{$attribute->id}}'>{{$attribute->title}}</option>
                                                                @endforeach
                                                            @endif

                                                        </select>
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                    </div>


                    <div class="row">
                        <div class="col-6">
                            <label>Descrizione Breve</label>
                            <div>
                                <div data-field="#description"
                                     class="summernote">{!!$record->description!!}</div>
                                <input id="description" type="hidden" name="item[description]"
                                       value="{{$record->description}}">
                            </div>
                        </div>

                        <div class="col-6">
                            <label>Descrizione Lunga</label>
                            <div>
                                <div data-field="#note"
                                     class="summernote">{!!$record->note!!}</div>
                                <input id="note" type="hidden" name="item[note]"
                                       value="{{$record->note}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <label>Note Tecniche</label>
                            <div>
                                <div data-field="#techincalnote"
                                     class="summernote">{!!$record->meta('techincalnote')!!}</div>
                                <input id="techincalnote" type="hidden" name="meta[techincalnote]"
                                       value="{{$record->meta('techincalnote')}}">
                            </div>
                        </div>
                        <div class="col-6">
                            <label>Contenuto della Scatola</label>
                            <div>
                                <div data-field="#boxcontent"
                                     class="summernote">{!!$record->meta('boxcontent')!!}</div>
                                <input id="boxcontent" type="hidden" name="meta[boxcontent]"
                                       value="{{$record->meta('boxcontent')}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h2>SOCIAL</h2>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="input-file-now">Immagine Social</label>
                                        <input type="file" id="imgsocial" name="imgsocial"
                                               {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                               class="dropify"
                                               data-default-file="@if($record->meta('imgsocial')){{config('httpmedia')."ecommerce/prodotti/".$record->meta('imgsocial')}}@else{{''}}@endif"/>
                                        <input id="imgsocial-old" type="hidden" name="imgsocial-old"
                                               value="{{$record->meta('imgsocial')}}">
                                        <script>
                                            $(function () {
                                                // upload
                                                var __imghighlight = $("#imgsocial").dropify();
                                                __imghighlight.on('dropify.afterClear', function (event, element) {
                                                    $('#imgsocial-old').val('');
                                                });
                                            })
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(!$allIds)@include('ecommerce.product.meta')@endif

                    <div class="form-actions">
                        <hr style="margin-top: 0px"/>
                        <div class="row">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{$record->id_category}}" name="item[id_category]"
                           id="idCategory"/>

                    <input type="hidden" value="{{$categorySecond}}" name="catsec" id="secCat"/>

                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label title="Catergoria Madre"
                           class="control-label">Categoria</label>
                </div>
                <div class="myadmin-dd-empty dd" id="nestable">

                    @if($catRoot)
                        <ol class="dd-list">
                            <li class="dd-item" data-id="0" id="step-0">
                                <div class="dd3-content @if($record->id_category==0) dd3-active @endif"> Nessuna
                                    Categoria
                                </div>
                            </li>
                            @foreach($catRoot as $cat)
                                <li class="dd-item" data-id="{{$cat->id}}" id="step-{{$cat->id}}">
                                    <div class="dd3-content @if($cat->id==$record->id_category) dd3-active @endif">
                                        <a href="#">
                                            {{$cat->title}}
                                        </a>
                                        <div class="btn-group btn-tree-list" role="group" aria-label="...">
                                            <a href="#" data-idcat="{{$cat->id}}"
                                               class="@if(in_array($cat->id, $arrayCategorySec)) active @endif"><span
                                                        class='fa fa-star'></span></a>
                                        </div>
                                    </div>
                                    @if(count($cat->categoryChild)>0)
                                        <ol class="dd-list">
                                            @foreach($cat->categoryChild as $catLivUno)
                                                <li class="dd-item dd3-item" data-id="{{$catLivUno->id}}"
                                                    id="step-{{$catLivUno->id}}">
                                                    <div class="dd3-content @if($catLivUno->id==$record->id_category) dd3-active @endif">
                                                        <a href="#">
                                                            {{$catLivUno->title}}
                                                        </a>
                                                        <div class="btn-group btn-tree-list" role="group"
                                                             aria-label="...">
                                                            <a href="#" data-idcat="{{$catLivUno->id}}"
                                                               class="@if(in_array($catLivUno->id, $arrayCategorySec)) active @endif"><span
                                                                        class='fa fa-star'></span></a>
                                                        </div>
                                                    </div>
                                                    @if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)
                                                        <ol class="dd-list">
                                                            @foreach($catLivUno->categoryChild as $catLivDue)
                                                                <li class="dd-item dd3-item "
                                                                    data-id="{{$catLivDue->id}}"
                                                                    id="step-{{$catLivDue->id}}">
                                                                    <div class="dd3-content @if($catLivDue->id==$record->id_category) dd3-active @endif">
                                                                        <a href="#">
                                                                            {{$catLivDue->title}}
                                                                        </a>
                                                                        <div class="btn-group btn-tree-list"
                                                                             role="group" aria-label="...">
                                                                            <a href="#"
                                                                               data-idcat="{{$catLivDue->id}}"
                                                                               class="@if(in_array($catLivDue->id, $arrayCategorySec)) active @endif"><span
                                                                                        class='fa fa-star'></span></a>
                                                                        </div>
                                                                    </div>
                                                                    @if($catLivDue->categoryChild && count($catLivDue->categoryChild)>0)
                                                                        <ol class="dd-list">
                                                                            @foreach($catLivDue->categoryChild as $catLivTre)
                                                                                <li class="dd-item dd3-item "
                                                                                    data-id="{{$catLivTre->id}}"
                                                                                    id="step-{{$catLivTre->id}}">
                                                                                    <div class="dd3-content @if($catLivTre->id == $record->id_category) dd3-active @endif">
                                                                                        <a href="#">
                                                                                            {{$catLivTre->title}}
                                                                                        </a>
                                                                                        <div class="btn-group btn-tree-list"
                                                                                             role="group"
                                                                                             aria-label="...">
                                                                                            <a href="#"
                                                                                               data-idcat="{{$catLivTre->id}}"
                                                                                               class="@if(in_array($catLivTre->id, $arrayCategorySec)) active @endif"><span
                                                                                                        class='fa fa-star'></span></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            @endforeach
                                                                        </ol>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                        </ol>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>

    @yield('modal')
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/theme/dracula.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/css/nestable.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>


    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <!-- cedemirror -->
    <script src="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/xml/xml.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/javascript/javascript.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/css/css.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/htmlmixed/htmlmixed.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/php/php.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/addon/edit/matchbrackets.js')}}"></script>
    <script src="{{asset('assets/js/jquery.nestable.js')}}"></script>

    <!-- Summernote -->
    <script src="{{asset('assets/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>

    <!-- Dropify -->
    <script src="{{asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>

      var arrayCat = [{{$categorySecond}}];
      // Select Choosen
      $(function () {

        $('.select2-multiple').select2();

        $.fn.nestableShow = function () {
          return this.each(function () {
            var $parents = $(this).parents();
            $parents.each(function (i) {
              var list = $(this).data("nestable");
              if (list) {
                $parents.slice(0, i).filter(list.options.itemNodeName).each(function () {
                  list.expandItem($(this));
                });
                return false;
              }
            });
          });
        };


        $.ajax({
          url: 'https://api.github.com/emojis',
          async: false
        }).then(function (data) {
          window.emojis = Object.keys(data);
          window.emojiUrls = data;
        });
        ;


        $('.summernote').summernote({
          height: 350, // set editor height
          minHeight: null, // set minimum height of editor
          maxHeight: null, // set maximum height of editor
          codemirror: { // codemirror options
            lineNumbers: true,
            theme: 'dracula',
            mode: "php"
          },
          hint: {
            match: /:([\-+\w]+)$/,
            search: function (keyword, callback) {
              callback($.grep(emojis, function (item) {
                return item.indexOf(keyword) === 0;
              }));
            },
            template: function (item) {
              var content = emojiUrls[item];
              return '<img src="' + content + '" width="20" /> :' + item + ':';
            },
            content: function (item) {
              var url = emojiUrls[item];
              if (url) {
                return $('<img />').attr('src', url).css('width', 20)[0];
              }
              return '';
            }
          },
          focus: false // set focus to editable area after initializing summernote
        });

        // summernote.change
        $('.summernote').on('summernote.change', function (we, contents, $editable) {
          $($(this).data('field')).val($(this).summernote('code'));
        });
        // al click del tasto salva...
        $('button[type="submit"]').on('click', function () {
          $('.summernote').each(function (summernote) {
            $($(this).data('field')).val($(this).summernote('code'));
          })
          return true;
        });

        /*
        $('#idCategory').change(function () {
            alert("cambio");
            var valOption = $(this).val();
            $('#content-feature').addClass("hide");
            $('#box-feature').html('');
            if (valOption.length > 0) {
                $.ajax({
                    method: "POST",
                    url: '{{ path_for('admin.ecommerce.producttype.ajax') }}',
                        data: {idCategory: valOption},
                        dataType: "html",
                        success: function (data) {
                            $('#content-feature').removeClass("hide");
                            $('#box-feature').html(data);
                        }
                    })
                }
            }); */


        $('#state_product').on('change', function () {
          var valOption = $(this).val();
          if (parseInt(valOption) === 2 || parseInt(valOption) === 3) {
            $('#box_data_arrivo').removeClass("hide");
          } else {
            $('#box_data_arrivo').addClass("hide");
          }
        });

        // Nestable
        var updateOutput = function (e) {
          var list = e.length ? e : $(e.target)
            , output = list.data('output');
          if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
          }
          else {
            output.val('JSON browser support required for this demo.');
          }
        };
        $('#nestable').nestable({
          group: 1,
          'collapseAll': true
        }).on('change', updateOutput);
        $('.dd').nestable('collapseAll');

        $('.dd3-content > a').click(function () {
          $('.dd3-content').removeClass('dd3-active');
          var id_cat_madre = $(this).parent().parent().data("id");
          $('#idCategory').val(id_cat_madre);
          $(this).parent().addClass("dd3-active");

          $('#content-feature').addClass("hide");
          $('#box-feature').html('');
          if (id_cat_madre > 0) {
            $.ajax({
              method: "POST",
              url: '{{ path_for('admin.ecommerce.producttype.ajax') }}',
              data: {idCategory: id_cat_madre},
              dataType: "html",
              success: function (data) {
                $('#content-feature').removeClass("hide");
                $('#box-feature').html(data);
              }
            })
          }
          return false;
        });

        $("a[data-idcat]").on("click", function (e) {
          $(this).toggleClass('active');
          var $id = $(this).data('idcat');
          var find = false;
          arrayCat = arrayCat.filter(function (value) {
            if (value !== $id) return value;
            else find = true;
          });
          if (!find) arrayCat.push($id);
          $("#secCat").val(arrayCat.join());
          return false;
        })

        $('#step-{{$record->id_category}}').nestableShow();

      });
    </script>
@endsection