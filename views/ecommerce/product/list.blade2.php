@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

        @include('ecommerce.product.header')

        <!-- START WIDGETS -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="table-responsive">
                            <table id="tabella" class="table m-t-30 table-hover contact-list">
                                <thead>
                                <tr id="thead-prodotti">
                                    <th><input type="checkbox" class="selectallrow"></th>
                                    <th>Prodotto</th>
                                    <th>Stato</th>
                                    <th>Permalink</th>
                                    <th>Categoria</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Prodotto</th>
                                    <th>Stato</th>
                                    <th>Permalink</th>
                                    <th>Categoria</th>
                                    <th class="no-filter">Azioni</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach ($table as $row)
                                    <tr>
                                        <td><input type="checkbox" class="selectrow" value="{{ $row->id }}"></td>
                                        <td>
                                            <a href=" {{ path_for('admin.ecommerce.product.update', ['id' => $row->id]) }}">
                                                {{ $row->title }}
                                            </a>
                                            </td>
                                        <td>
                                            @if ($row->state_product == 0)
                                                <span class="label label-success">Nuovo</span>
                                            @elseif ($row->state_product == 1)
                                                <span class="label label-warning">Usato</span>
                                            @else
                                                <span class="label label-info">In Arrivo</span>
                                            @endif
                                        </td>
                                        <td>{{ $row->permalink }}</td>
                                        <td>
                                            @if($row->id_category)
                                                {{$row->category->title}}
                                            @else
                                                nessuna categoria
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="...">
                                                <a href=" {{ path_for('admin.ecommerce.product.update', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                <a href="#" data-path="{{ path_for('admin.ecommerce.product.delete', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-danger delete-item"><span
                                                            class='fa fa-trash-o'></span></a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="deleteAll" data-path="{{ path_for('admin.ecommerce.product.deletegroup') }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script>
        $(document).ready(function () {

            var table = $('#tabella').DataTable({
                responsive: true,
                displayLength: 25,
                "order": 0,
                {{--
                language: {
                "url": "{{assetsBack}}lang/datatable/datatable-{% trans %}language{% endtrans %}.json"
                },
                --}}
                columns: [
                    {"searchable": false, "sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"searchable": false, "sortable": false}
                ]
            });

//la trovi in function_custom.js
            tabellaInit(table);
        });
    </script>
@endsection