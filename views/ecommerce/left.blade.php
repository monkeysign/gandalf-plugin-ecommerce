<li class="nav-small-cap m-t-10">--- ORDINI</li>
<li>
    <a href="{{path_for('admin.ecommerce.category.list')}}" class="waves-effect"><i
                class="zmdi zmdi-apps zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Ordini <span
                    class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <li><a href="{{path_for('admin.ecommerce.order.list')}}">Vai alla lista </a></li>
    <!--<li><a href="{{path_for('admin.ecommerce.order.add')}}">Aggiungi Nuova </a></li>-->
    </ul>
</li>
<li class="nav-small-cap m-t-10">--- ECOMMERCE</li>
<li>
    <a href="#" class="waves-effect"><i
                class="zmdi zmdi-mood zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Modulo Prodotti <span
                    class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <l1 class="text-center"><hr><h5><strong>Modulo Prodotti</strong></h5></l1>
        <li>
            <a href="{{path_for('admin.ecommerce.product.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-assignment zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Prodotti <span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.ecommerce.product.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.ecommerce.product.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
        <l1 class="text-center"><hr><h5><strong>Modulo Categorie</strong></h5></l1>
        <li>
            <a href="{{path_for('admin.ecommerce.category.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-apps zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Categorie <span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.ecommerce.category.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.ecommerce.category.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
        <li>
            <a href="{{path_for('admin.ecommerce.producttype.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-apps zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Tipologia Prodotti <span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.ecommerce.producttype.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.ecommerce.producttype.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
        <li>
            <a href="{{path_for('admin.ecommerce.feature.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-shape zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Caratteristiche<span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.ecommerce.feature.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.ecommerce.feature.add')}}">Aggiungi Nuova </a></li>

            </ul>
        </li>
        <li>
            <a href="{{path_for('admin.ecommerce.attribute.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-money zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Attributi<span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.ecommerce.attribute.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.ecommerce.attribute.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
    </ul>
</li>

<li>
    <a href="#" class="waves-effect"><i class="zmdi zmdi-mood zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Modulo Brand <span
                    class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <li><a href="{{path_for('admin.ecommerce.brand.list')}}">Vai alla lista </a></li>
        <li><a href="{{path_for('admin.ecommerce.brand.add')}}">Aggiungi Nuova </a></li>
    </ul>
</li>



