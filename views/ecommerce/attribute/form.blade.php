@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.attribute.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>Visualizzazione Attributo</strong>
                            @else
                                Nuovo Attributo
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.ecommerce.attribute.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Attributo *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->title}}" required
                                                       data-toggle="validator" type="text" name="item[title]"
                                                       id="name" class="form-control"
                                                       placeholder="Nome">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Caratteristica *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[id_feature]"
                                                        data-placeholder="Seleziona la caratteristica"
                                                        class="select2 select-choose form-control">
                                                    @foreach($allFeatures as $feature)
                                                        <option @if($feature->id==$record->id_feature){{"selected"}}@endif value="{{$feature->id}}">{{$feature->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Stato Pratica"
                                                    class="control-label">Stato</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[status]" class="form-control select-choose">
                                                    <option
                                                            @if ( $record->status == 1)
                                                            selected @endif
                                                            value="1">Pubblicato
                                                    </option>
                                                    <option
                                                            @if ( $record->status == 2)
                                                            selected @endif
                                                            value="2">Bozza
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection