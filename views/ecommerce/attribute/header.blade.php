<div class="row bg-title hidden-xs hidden-sm">
    <div class="col-12">
        <h4 class="pull-left page-title">Attributi</h4>
        <a type="button" href="{{path_for('admin.ecommerce.attribute.add')}}" class="pull-left ml-5 btn btn-info btn-sm"><i class="fa fa-plus-square"></i> Aggiungi Nuovo</a>
        <a type="button" href="{{path_for('admin.ecommerce.attribute.list')}}"	class="pull-left ml-3 btn btn-default btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vai alla lista</a>
    </div>
</div>

<div class="row bg-title hidden-md hidden-lg hidden-xl">
    <div class="col-12 mb-3 text-center ">
        <h4 class="page-title">Attributi</h4>
    </div>
    <div class="col-12 text-center">
        <a type="button" href="{{path_for('admin.ecommerce.attribute.add')}}" class="mb-3 btn btn-info btn-sm"><i class="fa fa-plus-square"></i> Aggiungi Nuovo</a>
        <a type="button" href="{{path_for('admin.ecommerce.attribute.list')}}"	class="ml-3 mb-3 btn btn-default btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vai alla lista</a>
    </div>
</div>