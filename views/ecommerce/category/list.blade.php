@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.category.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Categorie</h3>
                        <div class="myadmin-dd-empty dd" id="nestable">
                            @if($catRoot)
                                <ol class="dd-list">
                                    @foreach($catRoot as $cat)
                                        <li class="dd-item " data-id="{{$cat->id}}">
                                            <div class="dd3-content"> {{$cat->title}}
                                                <div class="btn-group btn-tree-list" role="group" aria-label="...">
                                                    <a href=" {{ path_for('admin.ecommerce.category.update', ['id' => $cat->id]) }}"
                                                       class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                    <a href="#" data-path="{{ path_for('admin.ecommerce.category.delete', ['id' => $cat->id]) }}"
                                                       class="btn btn-sm btn-danger delete-item"><span
                                                                class='fa fa-trash-o'></span></a>
                                                </div></div>
                                            @if(count($cat->categoryChild)>0)
                                                <ol class="dd-list">
                                                    @foreach($cat->categoryChild as $catLivUno)
                                                        <li class="dd-item dd3-item" data-id="{{$catLivUno->id}}">
                                                            <div class="dd3-content"> {{$catLivUno->title}}
                                                                <div class="btn-group btn-tree-list" role="group" aria-label="...">
                                                                    <a href=" {{ path_for('admin.ecommerce.category.update', ['id' => $catLivUno->id]) }}"
                                                                       class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                                    <a href="#" data-path="{{ path_for('admin.ecommerce.category.delete', ['id' => $catLivUno->id]) }}"
                                                                       class="btn btn-sm btn-danger delete-item"><span
                                                                                class='fa fa-trash-o'></span></a>
                                                                </div>
                                                            </div>
                                                        @if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)
                                                            <ol class="dd-list">
                                                                @foreach($catLivUno->categoryChild as $catLivDue)
                                                                    <li class="dd-item dd3-item" data-id="{{$catLivDue->id}}">
                                                                        <div class="dd3-content"> {{$catLivDue->title}}
                                                                            <div class="btn-group btn-tree-list" role="group" aria-label="...">
                                                                                <a href=" {{ path_for('admin.ecommerce.category.update', ['id' => $catLivDue->id]) }}"
                                                                                   class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                                                <a href="#" data-path="{{ path_for('admin.ecommerce.category.delete', ['id' => $catLivDue->id]) }}"
                                                                                   class="btn btn-sm btn-danger delete-item"><span
                                                                                            class='fa fa-trash-o'></span></a>
                                                                            </div>
                                                                        </div>
                                                                        @if($catLivDue->categoryChild && count($catLivDue->categoryChild)>0)
                                                                            <ol class="dd-list">
                                                                                @foreach($catLivDue->categoryChild as $catLivTre)
                                                                                    <li class="dd-item dd3-item" data-id="{{$catLivTre->id}}">
                                                                                        <div class="dd3-content"> {{$catLivTre->title}}
                                                                                            <div class="btn-group btn-tree-list" role="group" aria-label="...">
                                                                                                <a href=" {{ path_for('admin.ecommerce.category.update', ['id' => $catLivTre->id]) }}"
                                                                                                   class="btn btn-sm btn-default"><span class='fa fa-pencil'></span></a>
                                                                                                <a href="#" data-path="{{ path_for('admin.ecommerce.category.delete', ['id' => $catLivTre->id]) }}"
                                                                                                   class="btn btn-sm btn-danger delete-item"><span
                                                                                                            class='fa fa-trash-o'></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ol>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ol>
                                                        @endif
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            @endif
                                        </li>
                                    @endforeach
                                </ol>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- START WIDGETS -->
            <div class="row">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="deleteAll" data-path="{{ path_for('admin.ecommerce.category.deletegroup') }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/css/nestable.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script src="{{asset('assets/js/jquery.nestable.js')}}"></script>
    <script>
        $(document).ready(function () {
            // Nestable
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target)
                    , output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                }
                else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            $('#nestable').nestable({
                group: 1,
                'collapseAll' :true
            }).on('change', updateOutput);
            $('.dd').nestable('collapseAll');

            $('#nestable').on('click', '.delete-item', function () {
                var buttonclick = $(this);
                if (confirm('Sei sicuro di voler cancellare questo record?')) {
                    $.ajax({
                        url: '' + $(this).data('path'),
                        dataType: "json",
                        success: function (response) {
                            if (response.result) {
                                location.reload(true);
                            } else {
                                alert('ERRORE NELLA CANCELLAZIONE');
                            }
                        }
                    });
                }
            });

        });
    </script>
@endsection