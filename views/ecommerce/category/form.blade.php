@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('ecommerce.category.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>Visualizzazione Categoria</strong>
                            @else
                                Nuova Categoria
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.ecommerce.category.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Categoria *</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->title}}"
                                                               data-toggle="validator" type="text" name="item[title]"
                                                               id="name" class="form-control"
                                                               placeholder="Nome">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>

                                        <!-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <label title="Catergoria Madre"
                                                           class="control-label">Categoria Madre </label>

                                            <div class="input-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="ti-settings"></i>
                                                    </div>
                                                    <select name="item[id_parent]" class="form-control select-choose">
                                                        <option value="0">- Seleziona la categoria -</option>
                                                        @foreach( $allCat as $cat)
                                            <option @if ( $record->id_parent == $cat->id ) selected
                                                                    @endif value="{{ $cat->id }}">{{ $cat->path_label }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>

                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>-->

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Pratica"
                                                            class="control-label">Stato</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[status]" class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->status == 1)
                                                                    selected @endif
                                                                    value="1">Pubblicato
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 2)
                                                                    selected @endif
                                                                    value="2">Bozza
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label title="Icona"
                                                           class="control-label">Ordine</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-star"></i>
                                                        </div>
                                                        <input value="{{$record->orders}}" type="text"
                                                               name="item[orders]" class="form-control"
                                                               placeholder="Ordine Menù">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row mt-4 mb-4">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Tipologia Prodotti"
                                                           class="control-label">Tipologia Prodotti </label>
                                                    <div class="input-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="ti-settings"></i>
                                                            </div>
                                                            <select name="item[id_product_type]"
                                                                    class="form-control select-choose">
                                                                <option value="">- Seleziona la tipologia -</option>
                                                                @foreach( $allType as $type)
                                                                    <option @if ( $record->id_product_type == $type->id ) selected
                                                                            @endif value="{{ $type->id }}">{{ $type->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label title="Icona"
                                                           class="control-label">Icona </label>
                                                    <small></small>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-star"></i>
                                                        </div>
                                                        <input value="{{$record->meta('icon')}}" type="text"
                                                               name="meta[icon]" id="icon" class="form-control"
                                                               placeholder="Icona Visualizzata">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label title="Icona"
                                                           class="control-label">Peso Ricerca </label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-star"></i>
                                                        </div>
                                                        <input value="{{$record->search}}" type="text"
                                                               name="item[search]" class="form-control"
                                                               placeholder="Peso Ricerca">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4 mb-4">
                                            <div class="col-4">
                                                <label for="input-file-now">Icona in evidenza</label>
                                                <input type="file" id="imgicon" name="imgicon"
                                                       {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                       class="dropify"
                                                       data-default-file="@if($record->meta('imgicon')){{config('httpmedia')."ecommerce/categorie/".$record->meta('imgicon')}}@else{{''}}@endif"/>
                                                <input id="imgicon-old" type="hidden" name="imgicon-old"
                                                       value="{{$record->meta('imgicon')}}">

                                                <script>
                                                    $(function () {
                                                        // upload
                                                        var __imgicon = $("#imgicon").dropify();
                                                        __imgicon.on('dropify.afterClear', function (event, element) {
                                                            $('#imgicon-old').val('');
                                                        });
                                                    })
                                                </script>
                                            </div>
                                            <div class="col-4">
                                                <label for="input-file-now">Immagine in evidenza</label>
                                                <input type="file" id="imghighlight" name="imghighlight"
                                                       {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                       class="dropify"
                                                       data-default-file="@if($record->meta('imghighlight')){{config('httpmedia')."ecommerce/categorie/".$record->meta('imghighlight')}}@else{{''}}@endif"/>
                                                <input id="imghighlight-old" type="hidden" name="imghighlight-old"
                                                       value="{{$record->meta('imghighlight')}}">

                                                <input value="{{$record->meta('link_imghighlight')}}" placeholder="Link" name="meta[link_imghighlight]" class="form-control mt-2 mb-3" />

                                                <script>
                                                    $(function () {
                                                        // upload
                                                        var __imghighlight = $("#imghighlight").dropify();
                                                        __imghighlight.on('dropify.afterClear', function (event, element) {
                                                            $('#imghighlight-old').val('');
                                                        });
                                                    })
                                                </script>
                                            </div>
                                            <div class="col-4">
                                                <label for="input-file-now">Banner Ricerca</label>
                                                <input type="file" id="bannerRicerca" name="bannerRicerca"
                                                       {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                       class="dropify"
                                                       data-default-file="@if($record->meta('bannerRicerca')){{config('httpmedia')."ecommerce/categorie/".$record->meta('bannerRicerca')}}@else{{''}}@endif"/>
                                                <input id="bannerRicerca-old" type="hidden" name="bannerRicerca-old"
                                                       value="{{$record->meta('bannerRicerca')}}">

                                                <input value="{{$record->meta('link_bannerRicerca')}}" placeholder="Link" name="meta[link_bannerRicerca]" class="form-control mt-2 mb-3" />
                                                <script>
                                                    $(function () {
                                                        // upload
                                                        var __bannerricerca = $("#bannerRicerca").dropify();
                                                        __bannerricerca.on('dropify.afterClear', function (event, element) {
                                                            $('#bannerRicerca-old').val('');
                                                        });
                                                    })
                                                </script>
                                            </div>
                                            @for($i = 1; $i < 4; $i++)
                                                <div class="col-4">
                                                    <label for="input-file-now">Banner Menu {{$i}}</label>
                                                    <input type="file" id="banner{{$i}}" name="banner{{$i}}"
                                                           {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                           class="dropify"
                                                           data-default-file="@if($record->meta('banner'.$i)){{config('httpmedia')."ecommerce/categorie/".$record->meta('banner'.$i)}}@else{{''}}@endif"/>
                                                    <input id="banner{{$i}}-old" type="hidden" name="banner{{$i}}-old"
                                                           value="{{$record->meta('banner'.$i)}}">

                                                    <input value="{{$record->meta('link_banner'.$i)}}" placeholder="Link" name="meta[link_banner{{$i}}]" class="form-control mt-2 mb-3" />
                                                    <script>
                                                        $(function () {
                                                            // upload
                                                            var __imghighlight = $("#banner{{$i}}").dropify();
                                                            __imghighlight.on('dropify.afterClear', function (event, element) {
                                                                $('#banner{{$i}}-old').val('');
                                                            });
                                                        })
                                                    </script>
                                                </div>
                                            @endfor
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="white-box">
                                                    <h2>Pannello SEO e SOCIAL</h2>
                                                    <div class="row">
                                                        <div class="col-8">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label title="Campo obbligatorio"
                                                                               class="control-label">Titolo</label>
                                                                        <div class="input-group">
                                                                            <input value="{{$record->meta('title')}}"
                                                                                   data-toggle="validator" type="text" name="meta[title]"
                                                                                   id="seo_title" class="form-control"
                                                                            >
                                                                        </div>
                                                                        <span class="help-block with-errors"> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label title="Campo obbligatorio"
                                                                               class="control-label">Descrizione</label>
                                                                        <div class="input-group">
                                                                            <textarea name="meta[description]" class="form-control" rows="5">{{$record->meta('description')}}</textarea>
                                                                        </div>
                                                                        <span class="help-block with-errors"> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <label for="input-file-now">Immagine Social</label>
                                                            <input type="file" id="imgsocial" name="imgsocial"
                                                                   {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                                   class="dropify"
                                                                   data-default-file="@if($record->meta('imgsocial')){{config('httpmedia')."ecommerce/categorie/".$record->meta('imgsocial')}}@else{{''}}@endif"/>
                                                            <input id="imgsocial-old" type="hidden" name="imgsocial-old"
                                                                   value="{{$record->meta('imgsocial')}}">
                                                            <script>
                                                                $(function () {
                                                                    // upload
                                                                    var __imghighlight = $("#imgsocial").dropify();
                                                                    __imghighlight.on('dropify.afterClear', function (event, element) {
                                                                        $('#imgsocial-old').val('');
                                                                    });
                                                                })
                                                            </script>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Catergoria Madre"
                                                   class="control-label">Categoria Madre</label>
                                        </div>
                                        <div class="myadmin-dd-empty dd" id="nestable">
                                            <input type="hidden" value="{{$record->id_parent}}" name="item[id_parent]"
                                                   id="idMadre"/>
                                            @if($catRoot)
                                                <ol class="dd-list">
                                                    <li class="dd-item" data-id="0" id="step-0">
                                                        <div class="dd3-content @if($record->id_parent==0) dd3-active @endif">
                                                            Nessuna Categoria
                                                        </div>
                                                    </li>
                                                    @foreach($catRoot as $cat)
                                                        <li class="dd-item" data-id="{{$cat->id}}"
                                                            id="step-{{$cat->id}}">
                                                            <div class="dd3-content @if($cat->id==$record->id_parent) dd3-active @endif"> {{$cat->title}}
                                                            </div>
                                                            @if(count($cat->categoryChild)>0)
                                                                <ol class="dd-list">
                                                                    @foreach($cat->categoryChild as $catLivUno)
                                                                        <li class="dd-item dd3-item"
                                                                            data-id="{{$catLivUno->id}}"
                                                                            id="step-{{$catLivUno->id}}">
                                                                            <div class="dd3-content @if($catLivUno->id == $record->id_parent) dd3-active @endif"> {{$catLivUno->title}}

                                                                            </div>
                                                                            @if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)
                                                                                <ol class="dd-list">
                                                                                    @foreach($catLivUno->categoryChild as $catLivDue)
                                                                                        <li class="dd-item dd3-item "
                                                                                            data-id="{{$catLivDue->id}}"
                                                                                            id="step-{{$catLivDue->id}}">
                                                                                            <div class="dd3-content @if($catLivDue->id==$record->id_parent) dd3-active @endif"> {{$catLivDue->title}}
                                                                                            </div>
                                                                                            @if($catLivDue->categoryChild && count($catLivDue->categoryChild)>0)
                                                                                                <ol class="dd-list">
                                                                                                    @foreach($catLivDue->categoryChild as $catLivTre)
                                                                                                        <li class="dd-item dd3-item"
                                                                                                            data-id="{{$catLivTre->id}}">
                                                                                                            <div class="dd3-content @if($catLivTre->id==$record->id_parent) dd3-active @endif"> {{$catLivTre->title}}</div>
                                                                                                        </li>
                                                                                                    @endforeach
                                                                                                </ol>
                                                                                            @endif
                                                                                        </li>
                                                                                    @endforeach
                                                                                </ol>
                                                                            @endif
                                                                        </li>
                                                                    @endforeach
                                                                </ol>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            @endif
                                        </div>
                                    </div>


                                </div>


                            </div>
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/css/nestable.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>
    <script src="{{asset('assets/js/jquery.nestable.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- Dropify -->
    <script src="{{asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $.fn.nestableShow = function () {
            return this.each(function () {
                var $parents = $(this).parents();
                $parents.each(function (i) {
                    var list = $(this).data("nestable");
                    if (list) {
                        $parents.slice(0, i).filter(list.options.itemNodeName).each(function () {
                            list.expandItem($(this));
                        });
                        return false;
                    }
                });
            });
        };
        // Select Choosen
        jQuery(document).ready(function () {
            $('.select2-multiple').select2();
        });

        $(document).ready(function () {

            // Nestable
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target)
                    , output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                }
                else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            $('#nestable').nestable({
                group: 1,
                'collapseAll': true
            }).on('change', updateOutput);
            $('.dd').nestable('collapseAll');

            $('.dd3-content').click(function () {
                $('.dd3-content').removeClass('dd3-active');
                var id_cat_madre = $(this).parent().data("id");
                $('#idMadre').val(id_cat_madre);
                $(this).addClass("dd3-active");
                return false;
            });

            $('#step-{{$record->id_parent}}').nestableShow();

        });
    </script>
@endsection

