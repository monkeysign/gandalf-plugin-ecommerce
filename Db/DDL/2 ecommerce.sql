/* Create Table BRAND */
CREATE TABLE `ecommerce_payment` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ecommerce_payment`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `ecommerce_payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;


INSERT INTO `ecommerce_payment` (`id`, `title`, `description`, `code`, `status`, `updated_at`, `created_at`, `deleted_at`)
VALUES (NULL, 'Bonifico Bancario', 'Descrizione: coordinate bonifixo XXXXXAAAAABBBB', 'bon', '1', CURRENT_TIMESTAMP, NULL, NULL);
INSERT INTO `ecommerce_payment` (`id`, `title`, `description`, `code`, `status`, `updated_at`, `created_at`, `deleted_at`)
VALUES (NULL, 'PayPal', 'Descr: Pagamenti sicuri e veloci', 'PayPal', '1', CURRENT_TIMESTAMP, NULL, NULL);



CREATE TABLE `ecommerce_shipment` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `free` float NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ecommerce_shipment`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `ecommerce_shipment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;


INSERT INTO `ecommerce_shipment` (`id`, `title`, `description`, `price`, `free`, `status`, `updated_at`, `created_at`, `deleted_at`)
VALUES (NULL, 'Spedizione Standard', 'Spedizione Corriere Standard', '6', '100', '1', CURRENT_TIMESTAMP, NULL, NULL);



/* ALTER PRODOTTI - modifica campo iso default it*/

ALTER TABLE `ecommerce_product_meta` CHANGE `iso` `iso` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'it';


/* ALTER pagamenti add cost per gestine costi paypal*/

ALTER TABLE `ecommerce_payment` ADD `cost` FLOAT NULL AFTER `status`;

UPDATE `ecommerce_payment` SET `cost` = '3', `created_at` = NULL, `deleted_at` = NULL WHERE `ecommerce_payment`.`id` = 2;

/* CREATE ecommerce_brand_meta */
CREATE TABLE `ecommerce_brand_meta` ( `id` int(11) DEFAULT NULL COMMENT 'Campo usato solo per Eloquent', `id_brand` int(11) NOT NULL, `iso` varchar(500) NOT NULL, `meta_key` varchar(500) NOT NULL, `value` text NOT NULL, `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `created_at` timestamp NULL DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1

ALTER TABLE `ecommerce_category` ADD `level` INT NOT NULL DEFAULT '0' AFTER `orders`;