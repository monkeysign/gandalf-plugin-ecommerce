CREATE TABLE `ecommerce_order_product` (
  `id` bigint(20) NOT NULL,
  `product` varchar(255) NOT NULL,
  `vat` float NOT NULL default 0,
  `quantity` int(11) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_order` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_order_product`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_order_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

CREATE TABLE `ecommerce_product_type` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint NOT NULL default 0,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_product_type`
  ADD PRIMARY KEY (`id`);

CREATE TABLE `ecommerce_product_type_feature` (
  `id` bigint(20) NOT NULL,
  `id_product_type` bigint(20) NOT NULL,
  `id_feature` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_product_type_feature`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_category` ADD `id_product_type` FLOAT NULL AFTER `id`;

ALTER TABLE `ecommerce_product_type` ADD `slug` VARCHAR(10) NOT NULL AFTER `title`;




