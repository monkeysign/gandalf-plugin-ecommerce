/* Create Table BRAND */
CREATE TABLE `ecommerce_brand` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ecommerce_brand`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `ecommerce_brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `ecommerce_category` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `orders` tinyint null,
  `status` int null,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ecommerce_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;


CREATE TABLE `ecommerce_feature` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `ecommerce_attribute`
--
ALTER TABLE `ecommerce_feature`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `ecommerce_attribute`
--
ALTER TABLE `ecommerce_feature`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `ecommerce_category_feature` (
  `id` bigint(20) NOT NULL,
  `id_category` bigint(20) NOT NULL,
  `id_feature` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_category_feature`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_category_feature`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;



CREATE TABLE `ecommerce_attribute` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_feature` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_attribute`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_attribute`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


CREATE TABLE `ecommerce_product` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `description` text NULL,
  `note` text NULL,
  `price` float NOT NULL,
  `novelty` tinyint NOT NULL DEFAULT '0',
  `offer` tinyint NOT NULL DEFAULT '0',
  `price_offer` float NOT NULL DEFAULT '0',
  `state_product` tinyint NOT NULL DEFAULT '0',
  `best_seller` tinyint NOT NULL DEFAULT '0',
  `quantity` int NULL,
  `ean` varchar(255) NULL,
  `file` varchar(255) NULL,
  `id_category` bigint(20) NULL,
  `id_brand` bigint(20) NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_product`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


  CREATE TABLE `ecommerce_product_attribute` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_attribute` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ecommerce_product_attribute`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecommerce_product_attribute`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;



CREATE TABLE IF NOT EXISTS `ecommerce_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fiscalcode` varchar(16) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `surname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `company` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `post_office` varchar(5) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `payment_status` smallint(6) NOT NULL DEFAULT '0',
  `payment_type` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL DEFAULT '0',
  `note` varchar(500) DEFAULT NULL,
  `date` datetime NOT NULL,
  `total` float NOT NULL,
  `payment_date` datetime DEFAULT NULL,
  `billing_code` varchar(25) DEFAULT NULL,
  `vat` varchar(11) DEFAULT NULL,
  `invoice_information` text,
   `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE `ecommerce_category_meta` (
  `id` int(11) DEFAULT NULL COMMENT 'Campo usato solo per Eloquent',
  `id_category` int(11) NOT NULL,
  `iso` varchar(500) NOT NULL,
  `meta_key` varchar(500) NOT NULL,
  `value` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ecommerce_category_meta`
  ADD PRIMARY KEY (`id_category`,`meta_key`);



ALTER TABLE `ecommerce_category` ADD `orders` TINYINT(4) NULL DEFAULT NULL;

ALTER TABLE `ecommerce_product` ADD `permalink` VARCHAR(255) NOT NULL AFTER `id_category`, ADD `type` TINYINT NOT NULL DEFAULT '0' AFTER `permalink`;


create table ecommerce_product_meta
(
  id int null,
  id_product int null,
  meta_key varchar(500) null,
  value varchar(21845) null,
  updated_at timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
  created_at timestamp default '0000-00-00 00:00:00' not null,
  iso tinytext null
)
  engine=InnoDB;
