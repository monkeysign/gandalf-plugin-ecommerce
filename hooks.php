<?php

//Qui hooks

/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
hooks()->add_filter( APP_VIEWS_PATH, function ( $views = [] ) {
	return array_merge( [ __DIR__ . '/views/' ], $views );
} );

/**
 * Aggiungo le voci al menu tramite una vista presente nel plugin
 */
hooks()->add_action( ADMIN_LEFT_MENU, function () {
    if(user_logged()->role <= 1) {
        view()->render('ecommerce.left', []);
    }
},18 );

/**
 * Includo gli helper
 */
require __DIR__ . '/helpers.php';