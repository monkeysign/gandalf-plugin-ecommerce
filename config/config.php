<?php
return [
	"name"   => 'ecommerce',
	"hasSub" => true,
	"sub" => [],
	"class"  => \Plugins\ECOMMERCE\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];